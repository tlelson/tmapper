from setuptools import setup
import os
from subprocess import Popen, PIPE

def readme():
    with open('README.md') as f:
        return f.read()

def get_version():
    p = Popen(['git', 'describe'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, err = p.communicate()
    if err:
        raise Exception("Problem getting git version: {}".format(err))
    output = output.decode("utf-8").replace('\n', '')
    try:
        ver, commits, shorthash = output.split('-')
    except ValueError:  # no commits after tag
        ver, commits, shorthash = output, '0', '00000000'

    # One day change 'commits' to: py35_0 or py27_0
    ver_string = "{}.{}".format(ver, commits)
    print('Pip version will be: ' + ver_string)
    return ver_string

setup(name='tmapper', # N.B this must be unique on PyPi
        ## Random text and information
        version=get_version(),
        description='Constructs printable maps suitable for overland compass navigation.',
        long_description=readme(),
        classifiers=[
            'Development Status :: 3 - Alpha',
            'License :: OSI Approved :: MIT License',
            'Programming Language :: Python :: 2.7/ 3.5',
            'Topic :: Mapping',
        ],
        keywords='',
        url='https://anaconda.org/tle/tmapper',
        author='tle',
        author_email='tim.l.elson@gmail.com',
        license='MIT',

        ## This is a list of the packages to include
        packages=['tmapper', 'utm', 'grequests', 'geomag'],  # The packages you want to include. use >>> setuptools.find_packages()
        include_package_data=True,  # Required for next line

        ## Package and Testing dependecies
        install_requires=[
            'pillow',
            'numpy',
            'lxml',
            'click',
            'requests',
            'gevent'
        ],
        tests_require=['pytest'],

        ## Console command access points
        entry_points={
            'console_scripts': ['topo-print=tmapper.cli_app:cli'],
        },
        zip_safe=False  # ??
)
