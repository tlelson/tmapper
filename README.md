# Topographic Map Maker

*Code runs on both python 2.7 and 3.5 but conda builds differently. See build section*

Aim:
**Create a printable topographic map from Lat/Long coordinate values that can be used for navigation with a compass.  This nessesitates gridlines**

## Usage:
See examples in `tmapper/examplesi/`

## About:

Tile servers exist that offer amazing maps for use on the web but climbers/bikers/explores must buy
expensive proprietary software in order to print out topographic maps.  This application allows
anyone to create printable, fully labeled maps from any tile servers resouces and plot their own
custom waypoints on it.

## Installation
This package is distributed by conda.  Install Anaconda or [Miniconda](http://conda.pydata.org/docs/install/quick.html) and use:

`$ conda install -c tle tmapper`


## Development

```
$ git clone https://tlelson@bitbucket.org/tlelson/tmapper.git
```

### Testing

Run the tests:

```bash
$ py.test
```

... do this for in both python 2.7 and 3.5 before commiting

### Building the Package:

1-  Build and test locally *before committing changes*. It is important to use the correct python environment for each package:
```bash
(general2) $ conda build conda-receipe_py35/
(general3) $ conda build conda-receipe_py27/
```

Activate a blank test environment
```
$ activate test3
(test3) $ conda install --use-local tmapper
```

2-  Commit the changes and Incriment the git tag if new major version.:

```bash
$ git tag -a X.X -m "Reason for new major/minor version"
```

3-  Push to Anaconda.org:

```
$ anaconda upload /tmp/osx-64/tmapper-1.4-16_gb77a06e_py27_0.tar.bz2
```

It is important to commit before building because the version number is derived from the git tag and the last commit hash.  This is done automatically by the setup.py and the meta.yaml.  If a package is build and pushed to anaconda.org before all changes are commited.  When that package causes some problems the bugs may be in the next commit since they were not commited.  This makes traking errors harder.

## TODO:

### Verify Accuracy of following Assumptions:
- The approximation of the lat and long works surprisingly well at zoomlevel 13+.  Find a zoom level
where it stops being valid.  Is it worth implimenting a more robust system?


### Quality Assurance Issues
**Quick**
- Change Tile to be its own class, impliment a method 'get_top_left' (from tile2deg) and 'get_bot_right'
- Update self.start_coord_br to be actuall lower corner not lower 'limit' point
- move staticmethods (start in Coordinate) to utils.py

**More Complex**
- MGRS
- Write grid labels outside map bounds
- Allow user input tile server
- target A4 ratio, rotate image if nessesary
- add top left degrees
- Think about takeing less tiles and resizing them up so that contours are readable but enough pixels
to write on it clearly.  Perhaps have a fixed minimum pixels for the legend (too look good) and increase
the size (never shrink - as the legend is relative) to meet it.

**Bug Fixes**
- Centralise UTM zone labels at high latitudes
- Deal with Scando UTM zones
- Find a zoom level where assumption of constant lon (for each lat) breakdown

### V1.5 - Ready for GoTopo


### V1.4 (Usability Testing)
**IN PROGRESS**

- Mark waypoints in different color
- Review dimesions of the detail block and grid adjustment text.
    Idea:
        - A map image must be scaled to fit on an A4 sheet anyway. (60% -30% scaling is optimal)
        - Thus, if we resize an image to be larger, It will not effect the printed map because the
        ammount of scalling for printing will just increase to fit it on the page
        - However! An increase in size will allow us more pixels to print Text and waypoints, with
        the clarity we got used to with the higher zoom level maps
        - !!! Requires a wrapping self.latitude at pixel into get_latitude_at_pixel(). This way if
        we scaled up by a factor of two only this method would need to deal with it rather than
        every time the raw numpy array is used.  If we make it: get_coord_at_pixel() and require
        a tuple as a pixel, then we are better placed in future to change the method by which
        we create the pixel-coord reference map ... if needed

**DONE**
Minimum Usability
- Verified Declination and UTM grid adjustment.  Changed grid_convergence to true2grid to allow adjustments
- Add declination       DONE
- Add UTM grid convergence (GN->TN)
- Add degree scale      DONE

- Reduce minimum tile setting (currently too many tiles for A4 print)
- Allow manual input of Map name (Output file?)
- Top left coordinate being updated to actual map corner rather than input specification
- Allow UTM grid lines
- Fit combined map image to a fixed border template
- Add logo
- Add Scale
- remove _pix_for_long and _pix_for_lat
- Can add grid lines and mark waypoints before OR after adding a legend and dms
Extras
- Remove prompt for grid, provide a '--no-grid'
- Build and verify CLI
- CLI should name the map the output file name if not set


### V0 (Development Version)
- Impliment logger to that progress of long methods such as build_image can be monitored
- Create pixel <-> Coordinate map
- Print border
- Determine whole Lat/Long to mark grid lines at
- Print grid
- Label grid lines
- Release
- Read in KML file from googlemaps
- Add polygons at waypoints
- Add text label at polygons
- Auto scale from 'limit' points
- auto choose zoom level
- Package up for distribution
- Basic command line app
- tests
- fixups
- Write interactive prompts for command line app (all user specification of boundry)



