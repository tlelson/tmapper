# Import without having to conda install/develop
from tmapper import TMap

# Create a new tmap
tmap = TMap(loglevel='INFO')

xml_text = None
#with open('/Users/minmac/Downloads/FlagstoneCreek-nolimits.kml') as f:
with open('/Users/minmac/Downloads/MainRange-small.kml') as f:
    xml_text = f.read()

tmap.load_waypoints_from_xml_string(xml_text)

# 1. Look at output before legend
tmap.build_image_async()
tmap.mark_all_waypoints()
tmap.add_grid('utm')
tmap.add_dms_border()
tmap.set_title("YoYOO")
tmap.add_legend()

tmap.show_map()
tmap.save_map('/Users/minmac/Desktop/FlagstoneCreek')

####################### DEV ###########################
tmap.modified_map_image.size

tmap.latitude_at_pixel
tmap.longditude_at_pixel

