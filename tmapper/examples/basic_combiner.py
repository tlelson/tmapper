from PIL import Image, ImageDraw

first = Image.open("examples/15128_9526.png")
second = Image.open("examples/15128_9527.png")
third = Image.open("examples/15129_9526.png")
forth = Image.open("examples/15129_9527.png")


# Combine the images
combined_image = Image.new("RGBA", (256*2, 256*2))
combined_image.paste(first, (0,0))
combined_image.paste(second, (0,256))
combined_image.paste(third, (256,0))
combined_image.paste(forth, (256,256))

combined_image.show()