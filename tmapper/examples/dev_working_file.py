# Import without having to conda install/develop
from tmapper import TMap

# Create a new tmap
tmap = TMap(loglevel='INFO')

# tmap.define_bounds(latitude=[-10, 10], longditude=[160, 120])  # zoom level 5 => 10 tiles
# tmap.define_bounds(latitude=[-10, 20], longditude=[-40., 10.])  # Large
# tmap.load_waypoints_from_file('/Users/minmac/Code/tmapper/tmapper/tests/Vail.kml')
# tmap.load_waypoints_from_file('/Users/minmac/Code/tmapper/tmapper/tests/MainRange-small.kml')
tmap.load_waypoints_from_file('/Users/minmac/Downloads/FlagstoneCreek.kml')

# Reseting bounds after loading waypoints works
# tmap.load_waypoints_from_file('/Users/minmac/Downloads/MissMathersHouse.kml')
# tmap.define_bounds(latitude=[36.048, 35.844], longditude=[52.032, 52.212])  # MtDamavand

## Comparing grid_divergence in extrema cases (UTM-CG-test-map)
# tmap.load_waypoints_from_file('/Users/minmac/Downloads/UTM-Overview.kml')
# tmap.load_waypoints_from_file('/Users/minmac/Downloads/TestArea-Left-S.kml')
# tmap.load_waypoints_from_file('/Users/minmac/Downloads/TestArea-Right-S.kml')
# tmap.load_waypoints_from_file('/Users/minmac/Downloads/TestArea-Left-N.kml')
# tmap.load_waypoints_from_file('/Users/minmac/Downloads/TestArea-Right-N.kml')

# 1. Look at output before legend
# tmap.build_image_sync()
tmap.build_image_async()

# UTM Grid convergence test
tmap.erase_edits()

tmap.mark_all_waypoints()
tmap.add_grid('utm')
# tmap.add_grid('degree')
tmap.add_dms_border()
tmap.add_legend()

tmap.show_map()
# tmap.save_map('/Users/minmac/Desktop/Vail')

####################### DEV ###########################


