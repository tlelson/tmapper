import unittest
import os
from tmapper import TMap, TMapException, Coordinate
import numpy as np

test_waypoints = ['Stern - cave', '?Lower Panorama', '?Mt Asplenium', '?Spicers E.peak', '?Governor Chair Lookout',
             '?Mt Mitchell', 'Emu Vale - Road Fork', 'Davies ridge - trail start', '?Funnel (Steamers)',
             'Lower Emu Vale Car park', '?Spicers W.peak', '?Access Lizard via NE ridge', 'Ridge Camp (Water)',
             '?Mt Huntley', 'Grassy knoll', 'Leave road for Davies', 'TestPoint', '?Prow (Steamers)',
             'Davies ridge camp', 'Mt Steamer', 'Bivi cave', '?Suspected location of Davies ridge camp',
             'Lizard point']

## TODO:
#   - Check that pixel locations are updated by offsets in:
#       - Markwaypoints
#       - calc_degree_grids
#       - calc_utm_grids
#   ... after the process' of:
#       - Adding DMS border
#       - adding legend


## Phase 1 tests

def compare_waypoint_px_post_resize(test_instance, msg, test_coord, tmap_orig, tmap_resized, magnification):
    px_nomag = tmap_orig.coord_to_pixel(test_coord)
    px_mag = tmap_resized.coord_to_pixel(test_coord)
    px_converted = tuple(map(lambda x: int(round(x / magnification)), px_mag))

    # A bit restrictive
    #test_instance.assertTupleEqual(px_nomag, px_converted, msg=msg)

    # Test the converted value is within range of magnification
    margin = int(magnification/2)

    for i in [0, 1]:
        if px_converted[i] not in range(px_nomag[i] - margin, px_nomag[i] + margin + 1):
            raise AssertionError(msg)

def assert_waypoints_equal(pts1, pts2):

    msg = "waypoints not equal: "

    if len(pts1) != len(pts2):
        raise AssertionError(msg)

    def compare_keys(ref_pts, test_pts):
        for k in ref_pts:
            if k not in test_pts:
                raise AssertionError(msg + "{} not found".format(k))

    compare_keys(pts1, pts2)
    compare_keys(pts2, pts1)

    for k in pts1:
        #[(x, y) for x in l1 for y in l2]  # any order
        #[(x, y) for x, y in zip(l1, l2)]  # if orders are correct

        for c1, c2 in zip(pts1[k], pts2[k]):
            c1.assertEqualTo(c2)

class Test_TMap_Phase1(unittest.TestCase):

    def setUp(self):
        self.tm = TMap()

        # Test file
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.fn = 'MainRange-small.kml'
        self.tm.load_waypoints_from_file(os.path.join(self.path, self.fn))

        zoom = 14
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)
        return

    def test_init(self):

        tm = TMap()

        self.assertFalse(tm._zoom)
        self.assertFalse(tm.start_coord_br)
        self.assertFalse(tm.start_coord_tl)
        self.assertFalse(tm.raw_map_image)
        self.assertFalse(tm.modified_map_image)
        self.assertFalse(tm.map_name)
        self.assertFalse(tm.kml_file_name)

        self.assertFalse(tm.latitude_at_pixel)
        self.assertFalse(tm.longditude_at_pixel)
        self.assertFalse(tm.waypoints)

    def test_text_load(self):
        tm = TMap()

        # Test file
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.fn = 'MainRange-small.kml'

        xml_text = None
        with open(os.path.join(self.path, self.fn)) as f:
            xml_text = f.read()
        tm.load_waypoints_from_xml_string(xml_text)

        zoom = 14
        firsttile, lasttile, rows, columns, no_tiles = tm._count_tiles(zoom)

        # build the ref table
        tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

        # Assert all the atributes are equal to the one contructed with alternative
        self.assertTupleEqual(tm._count_tiles(zoom), self.tm._count_tiles(zoom))
        assert_waypoints_equal(tm.waypoints, self.tm.waypoints)

        return


    def test_loaded_waypoints_file_not_saved(self):

        self.assertListEqual(sorted(list(self.tm.waypoints.keys())), sorted(test_waypoints), msg='waypoints not all loaded')
        self.assertEqual(self.tm.map_name, 'Main Range', 'Map name not being set')
        self.assertEqual(self.tm.kml_file_name, self.fn, msg='kml file name not saved')

    def test_load_waypoints_from_missing_file(self):

        tm = TMap()

        # Test file
        path = os.path.dirname(os.path.realpath(__file__))
        fn = 'MainRange-missing.kml'
        with self.assertRaises(TMapException):  # OSError File not existing
            tm.load_waypoints_from_file(os.path.join(path, fn))

    def test_load_waypoints_from_bad_file(self):
        pass
        tm = TMap()

        # Test file
        path = os.path.dirname(os.path.realpath(__file__))
        fn = 'MainRange-broke.kml'
        with self.assertRaises(TMapException):  # Tag mismatch XMLSyntaxError
            tm.load_waypoints_from_file(os.path.join(path, fn))

    def test_define_bounds(self):

        tm = TMap()

        # 1 - Basic test
        tm.define_bounds(latitude=[-10, -12], longditude=[160, 175])
        tm.start_coord_tl.assertEqualTo(Coordinate(-10, 160))
        tm.start_coord_br.assertEqualTo(Coordinate(-12, 175))

        # 2 - extra values - cross equator and GMT line
        tm.define_bounds(latitude=[-10, 1.0, 20], longditude=[160, 20, -175])
        tm.start_coord_tl.assertEqualTo(Coordinate(20, -175))
        tm.start_coord_br.assertEqualTo(Coordinate(-10, 160))

        # 3 - Latitude out of range 1
        with self.assertRaises(TMapException):
            tm.define_bounds(latitude=[-10, 1.0, 93], longditude=[160, 20, -175])

        # 4 - Latitude out of range 2
        with self.assertRaises(TMapException):
            tm.define_bounds(latitude=[-10, 1.0, -93], longditude=[160, 20, -175])

        # 5 - Longditude out of range 1
        with self.assertRaises(TMapException):
            tm.define_bounds(latitude=[-10, 1.0], longditude=[190, 20, -175])

        # 6 - Longditude out of range 2
        with self.assertRaises(TMapException):
            tm.define_bounds(latitude=[-10, 1.0], longditude=[-190, 20, -175])


# Phase 2 tests

class Test_TMap_singlePoint(unittest.TestCase):
    """This uses a kml without 'limit' waypoints and only one waypoint
    """

    def setUp(self):
        self.tm = TMap()

        # Test file
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.fn = 'SinglePoint.kml'
        return

    def load_from_file(self):
        # Test loading as text buffer
        with self.assertraises(TMapException):
            self.tm.load_waypoints_from_file(os.path.join(self.path, self.fn))
        return

    def load_from_text(self):
        xml_text = None
        with open(os.path.join(self.path, self.fn)) as f:
            xml_text = f.read()

        with self.assertRaises(TMapException):
            self.tm.load_waypoints_from_xml_string(xml_text)

        return


class Test_TMap_nolimits(unittest.TestCase):
    """This uses a kml without 'limit' waypoints
    """

    def setUp(self):
        self.tm = TMap()

        # Test file
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.fn = 'FlagstoneCreek-nolimits.kml'

        # Test loading as text buffer
        # self.tm.load_waypoints_from_file(os.path.join(self.path, self.fn))

        xml_text = None
        with open(os.path.join(self.path, self.fn)) as f:
            xml_text = f.read()
        self.tm.load_waypoints_from_xml_string(xml_text)

        zoom = 14
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)
        return

    def test_mark_all_waypoints(self):
        # No need for an internet connection to build map, basically we just need a pixel ref table

        no_pts_marked = self.tm.mark_all_waypoints(testing=True)

        self.assertEqual(7, no_pts_marked, msg="Incorrect no of waypoints marked on this map!!")
        return

    def test_mark_all_waypoints2(self):
        # This must raise exception because image is not built

        msg=("Exception not raised when trying to mark waypoints before creating an image!!")
        with self.assertRaises(TMapException, msg=msg):
            self.tm.mark_all_waypoints(testing=False)

        return


class Test_TMap_MainRange(unittest.TestCase):

    def setUp(self):
        self.tm = TMap()

        # Test file
        self.path = os.path.dirname(os.path.realpath(__file__))
        self.fn = 'MainRange-small.kml'

        # Test loading as text buffer
        # self.tm.load_waypoints_from_file(os.path.join(self.path, self.fn))

        xml_text = None
        with open(os.path.join(self.path, self.fn)) as f:
            xml_text = f.read()
        self.tm.load_waypoints_from_xml_string(xml_text)

        zoom = 14
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)
        return

    def test_coord_to_pixel(self):
        test_coord1 = self.tm.waypoints['Lizard point'][0]
        px_point1 = self.tm.coord_to_pixel(test_coord1)
        self.assertTupleEqual((802, 320), px_point1, msg="Point on map mismarked or missing!")

        test_coord2 = self.tm.waypoints['?Governor Chair Lookout'][0]
        px_point2 = self.tm.coord_to_pixel(test_coord2)
        self.assertIsNone(px_point2, msg="Point off map returns wrong coordinates")

        test_coord3 = Coordinate(-27.0, 153.0)  # ~Brisbane
        px_point3 = self.tm.coord_to_pixel(test_coord3)
        self.assertIsNone(px_point3, msg="Point way off map returns wrong coordinates")

        return

    def test_coord_to_pixel_post_resize(self):
        """ Double image size, make sure pixel reference still good
        """

        # 1.  Setup new instance
        tm = TMap()
        tm.load_waypoints_from_file(os.path.join(self.path, self.fn))
        zoom = 14
        firsttile, lasttile, rows, columns, no_tiles = tm._count_tiles(zoom)

        # build the ref table
        tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

        # 2.  Increase size of image by 2 times
        magnification = 9.001  # 2.
        image_size = tuple(map(lambda x: int(x*magnification), [tm.longditude_at_pixel.size, tm.latitude_at_pixel.size]))
        tm._enlarge_px_ref_tables(magnification, image_size)

        msg="Resized referenence table inaccurate!"
        converter = lambda x: int(round(x / magnification))

        # Test real marked points
        test_coord = self.tm.waypoints['Lizard point'][0]
        compare_waypoint_px_post_resize(self, msg, test_coord, self.tm, tm, magnification)

        test_coord = self.tm.waypoints['Bivi cave'][0]
        compare_waypoint_px_post_resize(self, msg, test_coord, self.tm, tm, magnification)

        test_coord = self.tm.waypoints['Grassy knoll'][0]
        compare_waypoint_px_post_resize(self, msg, test_coord, self.tm, tm, magnification)

        test_coord = self.tm.waypoints['Mt Steamer'][0]
        compare_waypoint_px_post_resize(self, msg, test_coord, self.tm, tm, magnification)

        test_coord = self.tm.waypoints['TestPoint'][0]
        compare_waypoint_px_post_resize(self, msg, test_coord, self.tm, tm, magnification)

        # Test non-existant points
        test_coord = self.tm.waypoints['?Governor Chair Lookout'][0]
        px_nomag = self.tm.coord_to_pixel(test_coord)
        px_mag = tm.coord_to_pixel(test_coord)
        self.assertIsNone(px_nomag)
        self.assertIsNone(px_mag)

        test_coord = Coordinate(-27.0, 153.0)  # ~Brisbane
        px_nomag = self.tm.coord_to_pixel(test_coord)
        px_mag = tm.coord_to_pixel(test_coord)
        self.assertIsNone(px_nomag)
        self.assertIsNone(px_mag)

        return

    def test_mark_all_waypoints(self):
        # No need for an internet connection to build map, basically we just need a pixel ref table

        no_pts_marked = self.tm.mark_all_waypoints(testing=True)

        self.assertEqual(14, no_pts_marked, msg="Incorrect no of waypoints marked on this map!!")
        return

    def test_mark_all_waypoints2(self):
        # This must raise exception because image is not built

        msg=("Exception not raised when trying to mark waypoints before creating an image!!")
        with self.assertRaises(TMapException, msg=msg):
            self.tm.mark_all_waypoints(testing=False)

        return

class Test_TMap_MtDamavand(unittest.TestCase):
    """
    Testing basic internal functions using defined bounds not KML
    The bounds are manually defined.  The region is longer than it is wide (long_diff > lat_diff)
    """


    def setUp(self):
        self.tm = TMap()

        # Define bounds
        self.tm.define_bounds(latitude=[36.048, 35.844], longditude=[52.032, 52.212])

    def test_count_tiles1(self):
        zoom = 13
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        self.assertEqual(firsttile.zoomlvl, zoom)
        self.assertEqual(firsttile.column, 5280)
        self.assertEqual(firsttile.row, 3215)

        self.assertEqual(lasttile.zoomlvl, zoom)
        self.assertEqual(lasttile.column, 5284)
        self.assertEqual(lasttile.row, 3221)

        self.assertEqual(rows, 7)  # firsttile.column - lasttile.column
        self.assertEqual(columns, 5)

        self.assertEqual(no_tiles, rows * columns)

    def test_count_tiles2(self):
        zoom = 14
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        self.assertEqual(firsttile.zoomlvl, zoom)
        self.assertEqual(firsttile.column, 10560)
        self.assertEqual(firsttile.row, 6431)

        self.assertEqual(lasttile.zoomlvl, zoom)
        self.assertEqual(lasttile.column, 10568)
        self.assertEqual(lasttile.row, 6442)

        self.assertEqual(rows, 12)  # firsttile.column - lasttile.column
        self.assertEqual(columns, 9)

        self.assertEqual(no_tiles, rows * columns)

    def test_build_pixel2coord_ref_table1(self):
        zoom = 2
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        with self.assertRaises(TMapException, msg="Not checking for zoom level too large"):
            self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

    def test_build_pixel2coord_ref_table2(self):
        zoom = 14
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

        msg = "WARNING!! Incorrectly mapping Pixels to Coordinates !!"

        # Check Latitude
        target_lat_ref_table_start = np.array([36.04909896,  36.04902948,  36.04896])
        target_lat_ref_table_end = np.array([35.83586085, 35.83579137,  35.83572189])
        np.testing.assert_array_almost_equal(self.tm.latitude_at_pixel[:3], target_lat_ref_table_start, decimal=5, err_msg=msg)
        np.testing.assert_array_almost_equal(self.tm.latitude_at_pixel[-3:], target_lat_ref_table_end, decimal=5, err_msg=msg)
        self.assertEqual(self.tm.latitude_at_pixel.shape[0], 3072)

        # Check longditude
        target_long_ref_table_start = np.array([52.03125, 52.03133583, 52.03142166])
        target_long_ref_table_end = np.array([52.22874641, 52.22883224, 52.22891808])
        np.testing.assert_array_almost_equal(self.tm.longditude_at_pixel[:3], target_long_ref_table_start, decimal=5, err_msg=msg)
        np.testing.assert_array_almost_equal(self.tm.longditude_at_pixel[-3:], target_long_ref_table_end, decimal=5, err_msg=msg)
        self.assertEqual(self.tm.longditude_at_pixel.shape[0], 2304)

    def test_calculate_degree_grid_spacing(self):

        zoom = 13
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

        lat_grid_points, long_grid_points, grid_space, dp = self.tm._calculate_degree_grid_spacing(min_grids=2)

        self.assertEqual(grid_space, 0.1)

        np.testing.assert_array_almost_equal(lat_grid_points, np.array([36., 35.9]))
        np.testing.assert_array_almost_equal(long_grid_points, np.array([52.1, 52.2]))

    def test_coord_to_pixel(self):
        # TODO:
        pass

    def test_add_utm_grid_lines(self):

        zoom = 13
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

        self.tm.calc_utm_grids()
        gridres = self.tm._utm_grids

        self.assertListEqual(gridres.northing_vals.labels, [3990000, 3985000, 3980000, 3975000, 3970000, 3965000])
        self.assertListEqual(gridres.easting_vals.labels, [595000, 600000, 605000, 610000])

        self.assertListEqual(gridres.northing_vals.start_px, [(0, 120), (0, 444), (0, 769), (0, 1093), (0, 1418), (0, 1742)])
        self.assertListEqual(gridres.easting_vals.start_px, [(137, 0), (461, 0), (784, 0), (1108, 0)])

        self.assertListEqual(gridres.northing_vals.end_px, [(1280, 135), (1280, 459), (1280, 784), (1280, 1108), (1280, 1433), (1280, 1757)])
        self.assertListEqual(gridres.easting_vals.end_px, [(118, 1792), (441, 1792), (763, 1792), (1085, 1792)])

        self.assertIsNone(gridres.northing_vals.lab_px)
        self.assertIsNone(gridres.easting_vals.lab_px)

        return

    def test_add_deg_grid_lines(self):

        zoom = 13
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

        self.tm.calc_degree_grids()
        gridres = self.tm._deg_grids

        np.testing.assert_array_almost_equal(gridres.northing_vals.labels, np.array([36.050, 36.000, 35.950, 35.900, 35.850]))
        np.testing.assert_array_almost_equal(gridres.easting_vals.labels, np.array([ 52.05,  52.1 ,  52.15,  52.2 ,  52.25]))

        self.assertListEqual(gridres.northing_vals.start_px, [(0, 121), (0, 481), (0, 841), (0, 1200), (0, 1560)])
        self.assertListEqual(gridres.easting_vals.start_px, [(109, 0), (400, 0), (691, 0), (983, 0), (1274, 0)])

        self.assertListEqual(gridres.northing_vals.end_px, [(1280, 121), (1280, 481), (1280, 841), (1280, 1200), (1280, 1560)])
        self.assertListEqual(gridres.easting_vals.end_px, [(109, 1792), (400, 1792), (691, 1792), (983, 1792), (1274, 1792)])

        self.assertIsNone(gridres.northing_vals.lab_px)
        self.assertIsNone(gridres.easting_vals.lab_px)

        return

class Test_TMap_Vail(unittest.TestCase):
    """
    Testing basic internal functions using defined bounds not KML
    """

    def setUp(self):
        self.tm = TMap()

        # Test file
        path = os.path.dirname(os.path.realpath(__file__))
        fn = 'Vail.kml'
        self.tm.load_waypoints_from_file(os.path.join(path, fn))

        zoom = 14
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)
        return

    def test_count_tiles1(self):
        zoom = 12
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        self.assertEqual(firsttile.zoomlvl, zoom)
        self.assertEqual(firsttile.column, 837)
        self.assertEqual(firsttile.row, 1555)

        self.assertEqual(lasttile.zoomlvl, zoom)
        self.assertEqual(lasttile.column, 839)
        self.assertEqual(lasttile.row, 1557)

        self.assertEqual(rows, 3)  # firsttile.column - lasttile.column
        self.assertEqual(columns, 3)

        self.assertEqual(no_tiles, rows * columns)

    def test_count_tiles2(self):

        zoom = 13
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        self.assertEqual(firsttile.zoomlvl, zoom)
        self.assertEqual(firsttile.column, 1674)
        self.assertEqual(firsttile.row, 3111)

        self.assertEqual(lasttile.zoomlvl, zoom)
        self.assertEqual(lasttile.column, 1678)
        self.assertEqual(lasttile.row, 3114)

        self.assertEqual(rows, 4)  # firsttile.column - lasttile.column
        self.assertEqual(columns, 5)

        self.assertEqual(no_tiles, rows * columns)

    def test_build_pixel2coord_ref_table(self):

        # Check latituded
        target_lat_ref_table_start = np.array([39.67337039,  39.67330427,  39.67323815])
        target_lat_ref_table_end = np.array([39.53815466, 39.53808854,  39.53802242])

        np.testing.assert_array_almost_equal(self.tm.latitude_at_pixel[:3], target_lat_ref_table_start, decimal=5)
        np.testing.assert_array_almost_equal(self.tm.latitude_at_pixel[-3:], target_lat_ref_table_end, decimal=5)
        self.assertEqual(self.tm.latitude_at_pixel.shape[0], 2048)

        # Check longditude
        target_long_ref_table_start = np.array([-106.41357422, -106.41348839, -106.41340256])
        target_long_ref_table_end = np.array([-106.23805046, -106.23796463, -106.2378788])

        np.testing.assert_array_almost_equal(self.tm.longditude_at_pixel[:3], target_long_ref_table_start, decimal=5)
        np.testing.assert_array_almost_equal(self.tm.longditude_at_pixel[-3:], target_long_ref_table_end, decimal=5)
        self.assertEqual(self.tm.longditude_at_pixel.shape[0], 2048)

    def test_calculate_degree_grid_spacing(self):

        zoom = 13
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

        lat_grid_points, long_grid_points, grid_space, dp = self.tm._calculate_degree_grid_spacing()

        self.assertEqual(grid_space, 0.05)

        np.testing.assert_array_almost_equal(lat_grid_points, np.array([39.65, 39.6, 39.55]))
        np.testing.assert_array_almost_equal(long_grid_points, np.array([-106.4, -106.35, -106.3, -106.25]))

    def test_coord_to_pixel(self):
        test_coord1 = self.tm.waypoints['Vail Entrance'][0]
        px_point1 = self.tm.coord_to_pixel(test_coord1)
        self.assertTupleEqual((458, 500), px_point1, msg="Point on map mismarked or missing!")

        test_coord2 = self.tm.waypoints['Keystone'][0]
        px_point2 = self.tm.coord_to_pixel(test_coord2)
        self.assertIsNone(px_point2, msg="Point off map returns wrong coordinates")

        test_coord3 = Coordinate(-27.0, 153.0)  # ~Brisbane
        px_point3 = self.tm.coord_to_pixel(test_coord3)
        self.assertIsNone(px_point3, msg="Point way off map returns wrong coordinates")

        return

    def test_mark_all_waypoints(self):
        # No need for an internet connection to build map, basically we just need a pixel ref table

        no_pts_marked = self.tm.mark_all_waypoints(testing=True)

        self.assertEqual(5, no_pts_marked, msg="Incorrect no of waypoints marked on this map!!")
        return

    def test_mark_all_waypoints2(self):
        # This must raise exception because image is not built

        msg=("Exception not raised when trying to mark waypoints before creating an image!!")
        with self.assertRaises(TMapException, msg=msg):
            self.tm.mark_all_waypoints(testing=False)

        return

    def test_add_utm_grid_lines(self):

        self.tm.calc_utm_grids()
        gridres = self.tm._utm_grids

        self.assertListEqual(gridres.northing_vals.labels, [4390000, 4385000, 4380000])
        self.assertListEqual(gridres.easting_vals.labels, [380000, 385000, 390000])

        self.assertListEqual(gridres.northing_vals.start_px, [(0, 335), (0, 1016), (0, 1697)])
        self.assertListEqual(gridres.easting_vals.start_px, [(168, 0), (847, 0), (1526, 0)])

        self.assertListEqual(gridres.northing_vals.end_px, [(2048, 304), (2048, 986), (2048, 1667)])
        self.assertListEqual(gridres.easting_vals.end_px, [(199, 2048), (877, 2048), (1555, 2048)])

        self.assertIsNone(gridres.northing_vals.lab_px)
        self.assertIsNone(gridres.easting_vals.lab_px)

        return

    def test_add_deg_grid_lines(self):

        self.tm.calc_degree_grids()
        gridres = self.tm._deg_grids

        np.testing.assert_array_almost_equal(gridres.northing_vals.labels, np.array([ 39.65,  39.6 ,  39.55]))
        np.testing.assert_array_almost_equal(gridres.easting_vals.labels, np.array([-106.4 , -106.35, -106.3 , -106.25]))

        self.assertListEqual(gridres.northing_vals.start_px, [(0, 353), (0, 1109), (0, 1865)])
        self.assertListEqual(gridres.easting_vals.start_px, [(158, 0), (740, 0), (1323, 0), (1905, 0)])

        self.assertListEqual(gridres.northing_vals.end_px, [(2048, 353), (2048, 1109), (2048, 1865)])
        self.assertListEqual(gridres.easting_vals.end_px, [(158, 2048), (740, 2048), (1323, 2048), (1905, 2048)])

        self.assertIsNone(gridres.northing_vals.lab_px)
        self.assertIsNone(gridres.easting_vals.lab_px)

        return

class Test_TMap_Large(unittest.TestCase):
    """
    Testing basic internal functions using large scale spanning the equator and Grenich
    """

    def setUp(self):
        self.tm = TMap()
        self.tm.define_bounds(latitude=[-10, 20], longditude=[-40., 10.])

        # Dont need to get the image to test these
        zoom = 5
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)
        # build the ref table
        self.tm._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)
        return

    def test_count_tiles1(self):
        zoom = 6
        firsttile, lasttile, rows, columns, no_tiles = self.tm._count_tiles(zoom)

        self.assertEqual(firsttile.zoomlvl, zoom)
        self.assertEqual(firsttile.column, 24)
        self.assertEqual(firsttile.row, 28)

        self.assertEqual(lasttile.zoomlvl, zoom)
        self.assertEqual(lasttile.column, 33)
        self.assertEqual(lasttile.row, 33)

        self.assertEqual(rows, lasttile.row - firsttile.row + 1)
        self.assertEqual(columns, lasttile.column - firsttile.column + 1)

        self.assertEqual(no_tiles, rows * columns)
        return

    def test_build_pixel2coord_ref_table(self):

        # Check latituded
        target_lat_ref_table_start = np.array([21.94304553, 21.90018802, 21.85733051])
        target_lat_ref_table_end = np.array([-10.84295023, -10.88580775, -10.92866526])

        np.testing.assert_array_almost_equal(self.tm.latitude_at_pixel[:3], target_lat_ref_table_start, decimal=5)
        np.testing.assert_array_almost_equal(self.tm.latitude_at_pixel[-3:], target_lat_ref_table_end, decimal=5)
        self.assertEqual(self.tm.latitude_at_pixel.shape[0], 768)

        # Check longditude
        target_long_ref_table_start = np.array([-45., -44.95605469, -44.91210938])
        target_long_ref_table_end = np.array([11.11816406, 11.16210938, 11.20605469])

        np.testing.assert_array_almost_equal(self.tm.longditude_at_pixel[:3], target_long_ref_table_start, decimal=5)
        np.testing.assert_array_almost_equal(self.tm.longditude_at_pixel[-3:], target_long_ref_table_end, decimal=5)
        self.assertEqual(self.tm.longditude_at_pixel.shape[0], 1280)
        return

    def test_calculate_degree_grid_spacing(self):

        lat_grid_points, long_grid_points, grid_space, dp = self.tm._calculate_degree_grid_spacing()

        self.assertEqual(grid_space, 10)

        np.testing.assert_array_almost_equal(lat_grid_points, np.array([20., 10., 0., -10.]))
        np.testing.assert_array_almost_equal(long_grid_points, np.array([-40., -30., -20., -10.,   0.,  10.]))
        return

    def test_coord_to_pixel(self):
        # TODO:
        return

    def test_add_utm_grid_lines1(self):
        # Need to define again because we build the ref table in the setup
        tm = TMap()
        tm.define_bounds(latitude=[-10, 20], longditude=[-40., 10.])

        with self.assertRaises(TMapException, msg="Failing to check pixel reference table present"):
            tm.calc_utm_grids()
        return

    def test_add_utm_grid_lines(self):

        self.tm.calc_utm_grids()
        gridres = self.tm._utm_grids

        self.assertListEqual(list(gridres.northing_vals.labels), ['Q', 'P', 'N', 'M', 'L'])
        np.testing.assert_array_almost_equal(gridres.easting_vals.labels, np.array([23, 24, 25, 26, 27, 28, 29, 30, 31, 32]))

        self.assertListEqual(gridres.northing_vals.start_px, [(0, 138), (0, 325), (0, 512), (0, 698), (0, 767)])
        self.assertListEqual(gridres.easting_vals.start_px, [(68, 0), (204, 0), (341, 0), (477, 0), (614, 0), (750, 0),
                                                         (887, 0), (1023, 0), (1160, 0), (1279, 0)])

        self.assertListEqual(gridres.northing_vals.end_px, [(1280, 138), (1280, 325), (1280, 512), (1280, 698), (1280, 767)])
        self.assertListEqual(gridres.easting_vals.end_px, [(68, 768), (204, 768), (341, 768), (477, 768), (614, 768),
                                                       (750, 768), (887, 768), (1023, 768), (1160, 768), (1279, 768)])

        self.assertListEqual(gridres.northing_vals.lab_px, [(0, 45), (0, 232), (0, 419), (0, 605), (0, 791)])
        self.assertListEqual(gridres.easting_vals.lab_px, [(0, 0), (136, 0), (273, 0), (409, 0), (546, 0),
                                                           (682, 0), (819, 0), (955, 0), (1092, 0), (1228, 0)])
        # self.assertListEqual(gridres.northing_vals.lab_px, [(0, 44.5), (0, 231.5), (0, 418.5), (0, 604.5), (0, 791.5)])
        # self.assertListEqual(gridres.easting_vals.lab_px, [(0.0, 0), (136.0, 0), (273.0, 0), (409.0, 0), (546.0, 0),
        #                                                (682.0, 0), (819.0, 0), (955.0, 0), (1092.0, 0), (1228.0, 0)])

        return

    def test_add_deg_grid_lines(self):
        pass

class Test_TMap_Grids(unittest.TestCase):
    """
    Testing Grid mis-label bug (Issue #19).

    Here we test for logical matches (test_general_UTMs) and map specific points to ensure accuracy (test_specific_UTMs).
    """

    @classmethod
    def setUpClass(cls):
        cls.tmap = TMap(loglevel='WARN')

        # Test file
        path = os.path.dirname(os.path.realpath(__file__))
        fn = 'Guthega.kml'
        kml = os.path.join(path, fn)

        # Load in KML data
        cls.tmap.load_waypoints_from_file(kml)
        cls.tmap.build_image_async(testing=True)
        cls.tmap.add_grid('utm')

        return

    def test_general_UTMs(self):
        """Test applies to all maps"""

        northing_vals, easting_vals = self.tmap._utm_grids

        # Assertions
        assert len(northing_vals.labels) == len(northing_vals.start_px) == len(northing_vals.end_px), "labels should correspond to start and end pixel values"
        assert len(easting_vals.labels) == len(easting_vals.start_px) == len(easting_vals.end_px), "labels should correspond to start and end pixel values"

    def test_specific_UTMs(self):
        """Tests that apply to this map only"""

        northing_vals, easting_vals = self.tmap._utm_grids

        """  TARGET RESULTS:
        ipdb> self.tmap._utm_grids
        GridResults(
            northing_vals=DrawnLines(
                labels=[5986000, 5984000, 5982000, 5980000, 5978000],
                start_px=[(0, 574), (0, 1107), (0, 1641), (0, 2174), (0, 2707)],
                end_px=[(3144, 529), (3144, 1062), (3144, 1596), (3144, 2129), (3144, 2662)],
                lab_px=None),
            easting_vals=DrawnLines(
                labels=[620000, 622000, 624000, 626000, 628000],
                start_px=[(479, 0), (1010, 0), (1540, 0), (2071, 0), (2601, 0)],
                end_px=[(522, 3144), (1054, 3144), (1585, 3144), (2116, 3144), (2648, 3144)],
                lab_px=None)
        )
        """

        msg = "Inaccurate map!!"
        assert northing_vals.labels == [5986000, 5984000, 5982000, 5980000, 5978000] , msg
        assert northing_vals.start_px == [(0, 574), (0, 1107), (0, 1641), (0, 2174), (0, 2707)] , msg
        assert northing_vals.end_px == [(3144, 529), (3144, 1062), (3144, 1596), (3144, 2129), (3144, 2662)] , msg

        assert easting_vals.labels == [620000, 622000, 624000, 626000, 628000] , msg
        assert easting_vals.start_px == [(479, 0), (1010, 0), (1540, 0), (2071, 0), (2601, 0)] , msg
        assert easting_vals.end_px == [(522, 3144), (1054, 3144), (1585, 3144), (2116, 3144), (2648, 3144)] , msg

# Phase 3 tests

if __name__ == '__main__':
    unittest.main()
