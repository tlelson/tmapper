# Import without having to conda install/develop
from tmapper import TMap
from itertools import cycle

bounds = [
    {'latitude':[36.048, 35.844], 'longditude':[52.032, 52.212]},  # MtDamavand
    {'latitude':[-10, 10], 'longditude':[160, 120]},  # zoom level 5 => 10 tiles
    {'latitude':[-10, 20], 'longditude':[-40., 10.]}  # Large
]

kml_files = [
    '/Users/minmac/Code/tmapper/tmapper/tests/Vail.kml',
    '/Users/minmac/Code/tmapper/tmapper/tests/MainRange-small.kml'
]

grid_type = cycle(['utm', 'degree'])

for b in bounds:
    # Create a new tmap
    tmap = TMap(loglevel='INFO')

    tmap.define_bounds(**b)

    tmap.build_image_async()
    tmap.add_grid(next(grid_type))

    tmap.add_dms_border()
    tmap.add_legend()

    tmap.show_map()

    del tmap

for kml in kml_files:
    # Create a new tmap
    tmap = TMap(loglevel='INFO')

    tmap.load_waypoints_from_file(kml)

    tmap.build_image_async()
    tmap.add_grid(next(grid_type))
    tmap.mark_all_waypoints()

    tmap.add_dms_border()
    tmap.add_legend()

    tmap.show_map()

    del tmap


