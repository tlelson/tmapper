import unittest
from tmapper import Coordinate

class TestCoordinate(unittest.TestCase):
    def test_init1(self):
        # negative latitude
        lat = -81.2
        long = 10.0
        c = Coordinate(lat, long)
        self.assertEqual(c._northing, lat)
        self.assertEqual(c._easting, long)

    def test_init2(self):
        # lat out of bounds
        lat = -91.2
        long = -10.0
        with self.assertRaises(ValueError):
            Coordinate(lat, long)

        # lat out of bounds
        lat = 91.2
        long = -50.0
        with self.assertRaises(ValueError):
            Coordinate(lat, long)

    def test_init3(self):
        # lat out of bounds
        lat = -31.2
        long = -190.0
        with self.assertRaises(ValueError):
            Coordinate(lat, long)

        # lat out of bounds
        lat = -71.2
        long = 190.0
        with self.assertRaises(ValueError):
            Coordinate(lat, long)

    def test_assertEqualTo1(self):
        # equal coords, no exception

        c1 = Coordinate(56.7, -09.4)
        c2 = Coordinate(56.7, -09.4)

        self.assertEqual(c1.assertEqualTo(c2), 0)

    def test_assertEqualTo2(self):
        # non-equal coords, raise exception

        c1 = Coordinate(56.7, -09.4)
        c2 = Coordinate(46.7, -09.4)

        with self.assertRaises(AssertionError):
            c1.assertEqualTo(c2)

    def test_as_tile(self):
        # test value only.
        c1 = Coordinate(56.7, -09.4)
        res = c1.as_tile(14)

        self.assertEqual(res.zoomlvl, 14)
        self.assertEqual(res.column, 7764)
        self.assertEqual(res.row, 5044)

    def test_as_degrees(self):
        # test value only
        c1 = Coordinate(-56.7, 29.42)

        res = c1.as_degree()
        self.assertDictEqual(res.northing, {'degrees': -56.7})
        self.assertDictEqual(res.easting, {'degrees': 29.42})

    def test_as_dms(self):
        # test value only
        c1 = Coordinate(-56.505, 29.2525)

        res = c1.as_dms()
        self.assertEqual(res.northing['degrees'], -56)
        self.assertEqual(res.northing['minutes'], -30)
        self.assertAlmostEqual(res.northing['seconds'], -18)

        self.assertEqual(res.easting['degrees'], 29)
        self.assertEqual(res.easting['minutes'], 15)
        self.assertAlmostEqual(res.easting['seconds'], 9)

    def test_as_degmin(self):
        # test value only
        c1 = Coordinate(-56.505, 29.2525)

        res = c1.as_degmin()
        self.assertEqual(res.northing['degrees'], -56)
        self.assertAlmostEqual(res.northing['minutes'], -30.3)

        self.assertEqual(res.easting['degrees'], 29)
        self.assertAlmostEqual(res.easting['minutes'], 15.15)

    def test_deg2dms(self):

        # define test case
        angle = -34.2555

        # target
        d = int(angle)  # -34
        dec_m = 60 * (angle - int(angle))  # -15.33
        m = int(dec_m)
        s = 60 * (dec_m - m)

        res = Coordinate.deg2dms(angle)

        self.assertEqual(res['degrees'], d)
        self.assertEqual(res['minutes'], m)
        self.assertAlmostEqual(res['seconds'], s, places=2)
        return

    def test_deg2degmin(self):

        # define test case
        angle = -34.2555

        # target
        d = int(angle)  # -34
        m = 60 * (angle - int(angle))  # -15.33

        res = Coordinate.deg2degmin(angle)

        self.assertEqual(res['degrees'], d)
        self.assertAlmostEqual(res['minutes'], m, places=2)
        return

    def test_as_utms(self):
        # test value only
        pass

if __name__ == '__main__':
    unittest.main()
