from tmapper import TMap
import click
import os


@click.command()
@click.option('-k', '--kml', type=click.File('rb'),
              prompt='Please provide a kml file of waypoints',
              help=("KML file of points to plot on your map. Must include at least "
                    "two points named 'limit' to size the output map from. "))
@click.option('-o', '--outfile',
              help="Location of the constructed map. If not fully qualified, "
                   "it will be save in the current working dir.")
@click.option('--output-type', type=click.Choice(['GIF', 'PNG', 'JPEG']),
              default='GIF', help="Output type.")
@click.option('-z', '--zoomlevel', type=click.IntRange(0, 20), default=0,
              help="Request a specific zoomlevel.")
@click.option('--loglevel', type=click.Choice(['DEBUG', 'INFO', 'WARN']),
              default='INFO')
@click.option('--map-name', help="explictily set map name")
@click.option('--no-grid', is_flag=True, help="Print without grids")
@click.option('--no-degrees', is_flag=True, help="Print without degree border")
@click.option('--no-waypoints', is_flag=True, help="Print without waypoints")
def cli(kml, outfile, output_type, zoomlevel, loglevel, no_grid, no_degrees, no_waypoints, map_name):

    if not outfile:
        outfile = os.path.basename(kml.name).replace('.kml', '')
    elif not os.path.isabs(outfile):
        outfile = os.path.join(os.getcwd(), outfile)

    # Create a new tmap
    tmap = TMap(loglevel=loglevel)

    # Load in KML data
    tmap.load_waypoints_from_file(kml)
    tmap.build_image_async(zoom=zoomlevel)

    if not no_grid:
        tmap.add_grid('utm')
    if not no_degrees:
        tmap.add_dms_border()
    if not no_waypoints:
        tmap.mark_all_waypoints()

    tmap.set_title(map_name)
    tmap.add_legend()
    tmap.save_map(outfile, output_type.upper())

# Not required for release.  Entry point goes directly to cli()
if __name__ == '__main__':
    cli()
