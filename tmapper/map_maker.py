# -*- coding: utf-8 -*-

import logging
import sys
import os
import io
import grequests
import datetime
import utm
import geomag
from itertools import cycle, count
import lxml.etree
import numpy as np
from PIL import Image, ImageDraw, ImageOps, ImageFont
from tmapper.coordinate import Coordinate
from tmapper import utils
from collections import namedtuple

here = os.path.dirname(os.path.realpath(__file__))

DEGREE_SIGN = u'\N{DEGREE SIGN}'
MIN_MAP_SIDE_PXS = 3144.
API_KEY = '255de84ddef0420480e16762ba1dfc9b'  # Thunderforest API key

if sys.version_info.major < 3:
    unicode_str = unicode
else:
    unicode_str = str

class TMapException(Exception):
    pass


class TMap:

    def __init__(self, loglevel='WARN'):

        # Set up logger
        # TODO: use a global 'log' variable
        self.log = logging.getLogger('TMap')
        # create file handler which logs even debug messages
        fh = logging.FileHandler('tm.log')
        ch = logging.StreamHandler()
        if loglevel == 'INFO':
            self.log.setLevel(logging.INFO)
            ch.setLevel(logging.INFO)
        elif loglevel == 'WARN':
            self.log.setLevel(logging.WARN)
            ch.setLevel(logging.WARN)
        elif loglevel == 'DEBUG':
            self.log.setLevel(logging.DEBUG)
            ch.setLevel(logging.DEBUG)
        else:
            raise TMapException('Loglevel not implimented: ', loglevel)

        # Log all messages to the file
        fh.setLevel(logging.DEBUG)

        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        self.log.addHandler(fh)
        self.log.addHandler(ch)

        self.log.info('Logging setup')

        self.start_coord_tl = None
        self.start_coord_br = None
        self._zoom = 0
        self.raw_map_image = None  # Combined tiles without any markings
        self.modified_map_image = None
        self._magnification_factor = 1  # Integers only for now
        self.border_pxs = 0  # The number of pixels on combined_map between the edge and the actual map
        self.dms_bord_width = 0  # the number of pixels around map used for DMS markings

        # Tile servers data
        # use 'outdoors' here instead of 'landscape' because it seems to have everything
        # that 'landscape' has but also more trails.  Unfortunately no cliff lines indicated.
        self.server_url = cycle(['https://{}.tile.thunderforest.com/outdoors/'.format(x) for x in ['a', 'b', 'c']])
        self.tile_width = 256  # pixels
        self.tile_height = 256  # pixels

        self.latitude_at_pixel = np.array([])
        self.longditude_at_pixel = np.array([])

        # Data from KML
        self.map_name = None
        self.kml_file_name = None
        self.waypoints = {}

        # Grid marking results
        self._deg_grids = None
        self._utm_grids = None

        return

    def __repr__(self):
        pass

    def set_title(self, map_title):
        if ((self.modified_map_image.height - self.raw_map_image.height)
                != (self.modified_map_image.width - self.raw_map_image.width)):
            self.log.warning("Looks like the legend has already been added. Map name will not be changed ...")
        self.map_name = map_title
        return

    def load_waypoints_from_xml_string(self, xml_string):
        self.log.info("Attempting to load data from KML string:")

        try:
            if sys.version_info.major < 3:
                tree = lxml.etree.ElementTree(lxml.etree.fromstring(xml_string))
            else:
                tree = lxml.etree.ElementTree(lxml.etree.fromstring(xml_string.encode('utf-8')))
        except OSError:
            msg = "Requested file not found (Python3)"
            self.log.error(msg)
            raise TMapException(msg)
        except IOError:
            msg = "Requested file not found (Python2)"
            self.log.error(msg)
            raise TMapException(msg)
        except lxml.etree.XMLSyntaxError:
            msg = ("Error reading KML file.  If this has been downloaded from google maps "
                   "please send it to developers to investigate ... ")
            self.log.error(msg)
            raise TMapException(msg)

        try:  # as though a file pointer from click
            self.kml_file_name = None
        except AttributeError:  # not a file pointer, treat as a string
            self.kml_file_name = None

        self._parse_kml(tree)

        return

    def load_waypoints_from_file(self, filename):
        self.log.info("Attempting to load data from KML: {}".format(filename))

        try:
            tree = lxml.etree.parse(filename)
        except OSError:
            msg = "Requested file not found (Python3)"
            self.log.error(msg)
            raise TMapException(msg)
        except IOError:
            msg = "Requested file not found (Python2)"
            self.log.error(msg)
            raise TMapException(msg)
        except lxml.etree.XMLSyntaxError:
            msg = ("Error reading KML file.  If this has been downloaded from google maps "
                   "please send it to developers to investigate ... ")
            self.log.error(msg)
            raise TMapException(msg)

        try:  # as though a file pointer from click
            self.kml_file_name = os.path.basename(filename.name)
        except AttributeError:  # not a file pointer, treat as a string
            self.kml_file_name = os.path.basename(filename)

        self._parse_kml(tree)

        return

    def _parse_kml(self, tree):
        root = tree.getroot()
        NS = root.tag.split('}')[0] + '}'  # The xml namespace

        # Get Name
        map_name = tree.find('//{NS}name'.format(NS=NS)).text

        # Get all waypoint elements
        placemarks = tree.findall('//{NS}Placemark'.format(NS=NS))

        if len(placemarks) < 2:

            msg = ("Must be at least two waypoints to limit the size of the map.  "
                   "Waypoints named 'limit' will not be marked on the map")
            self.log.error(msg)
            raise TMapException(msg)

        waypoints = {}
        for mark in placemarks:

            # Get Waypoint name
            waypoint_name = mark.find('{NS}name'.format(NS=NS)).text

            # set limit of any capitalisation to lowercase
            if waypoint_name.lower() == 'limit':
                waypoint_name = 'limit'

            self.log.debug(u"Attempting to save waypoint: {}".format(waypoint_name))

            # Get waypoint coords
            try:
                vals = mark.find('{NS}Point'.format(NS=NS)).getchildren()
                # Get text from first coord tag
                filter_coords_only = lambda x: x.tag == '{NS}coordinates'.format(NS=NS)
                long, lat, _ = map(float, list(filter(filter_coords_only, vals))[0].text.split(','))
                self.log.debug(u'Coordinates found: ({}, {})'.format(lat, long))
            except AttributeError as e:
                msg = (u"Error finding coordinates for '{}' skipping!!: {}".format(waypoint_name, e))
                self.log.warning(msg)
                continue

            waypoints[waypoint_name] = waypoints.get(waypoint_name, []) + [Coordinate(lat, long)]

        if not self.map_name:
            self.map_name = map_name

        self.waypoints = waypoints
        self.log.info('Successfully registered {} waypoints with tmap instance ...'.format(len(waypoints)))

        # Look for limit waypoints and set map bounds with these, otherwise use furtherest waypoints
        if len(self.waypoints.get('limit', [])) >= 2:
            map_bounds = self.waypoints['limit']
        else:
            map_bounds = [i for sublist in self.waypoints.values() for i in sublist]

        long = [b.as_degree().easting['degrees'] for b in map_bounds]
        lat = [b.as_degree().northing['degrees'] for b in map_bounds]
        self.define_bounds(latitude=(max(lat), min(lat)), longditude=(min(long), max(long)))

        if len(self.waypoints.get('limit', [])):
            del self.waypoints['limit']  # Dont want to plot these

        return

    def mark_all_waypoints(self, testing=False):
        marked = 0

        if not len(self.waypoints):
            self.log.warning("No waypoints are loaded to mark!!")
            return marked

        for label in self.waypoints:
            points = self.waypoints[label]

            for point in points:  # facilitate multiple points sharing label name
                if self.mark_waypoint(point, label, testing=testing):
                    marked +=1

        self.log.info(u"{} waypoints successfully marked ...".format(marked))
        return marked

    def mark_waypoint(self, waypoint, label, color=(5, 33, 61), testing=False):
        self.log.debug(u"Attempting to mark waypoint '{}' at: {}".format(label, waypoint))

        # Get pixel coordinates - will sanitize incorrect format waypoint
        pixel_loc = self.coord_to_pixel(waypoint)

        # Skip ones off the map
        if not pixel_loc:
            self.log.debug('Waypoint not on this map ...')
            return False

        if testing:  # remove the need for an actual map to be built (thus test offline)
            return True

        # Calculate triangle corners - make it 100th the width of the whole side
        try:
            h = max(self.raw_map_image.size) / 120  # Triangle height
        except AttributeError:
            msg = ("A map image must be built before waypoints can be marked! "
                   "Use 'build_image_async()' to create a map image ...")
            self.log.error(msg)
            raise TMapException(msg)

        tri_pointA = int(pixel_loc[0] - h/2), int(pixel_loc[1] - h)
        tri_pointB = int(pixel_loc[0] + h/2), int(pixel_loc[1] - h)
        triangle_points = [pixel_loc, tri_pointA, tri_pointB]

        draw = ImageDraw.Draw(self.modified_map_image)
        draw.polygon(triangle_points, fill=color)

        # Add a label
        size = (int(h*30), int(h*1.2))
        self.add_label_to(self.modified_map_image, label, tri_pointB, size, rotation=-20)
        self.log.debug(u'Waypoint marked with polygon at ({},{})'.format(pixel_loc[0], pixel_loc[1]))

        return True

    def erase_edits(self):
        """
        Removes all waypoints, labels, gridline etc to return the compiled image without overlays
        :return: None
        """

        self.border_pxs = 0
        self.dms_bord_width = 0
        self._deg_grids = None
        self._utm_grids = None
        try:
            self.modified_map_image = self.raw_map_image.copy()
        except AttributeError:  # Already reset
            self.log.warning("No modifications to erase ...")
            pass
        return

    def get_tile_request_path(self, zoom, column, row):
        serv = next(self.server_url)
        img_url = serv + f'{zoom}/{column}/{row}.png'
        if API_KEY:
            img_url += f'?apikey={API_KEY}'
        return img_url

    def define_bounds(self, latitude=None, longditude=None):
        """
        Step 1.
        Defines the extent of the map.
        :param latitude: (int start, int stop)
        :param longditude: (int start, int stop)
        :return: None
        """
        # Store topleft and bottom right coordinates
        self.log.debug(u'Attempting to process user input '
                       u'map boundarys for Lat: {}, Long: {}'.format(latitude, longditude))
        try:
            self.start_coord_tl = Coordinate(max(latitude), min(longditude))
            self.start_coord_br = Coordinate(min(latitude), max(longditude))
        except ValueError as e:
            msg = (u'Check you Lat/Long. {}'.format(e))
            self.log.error(msg)
            raise TMapException(msg)

        self.log.info(u'Boundaries succesfully defined ...')
        return

    def _count_tiles(self, zoom_level):
        firsttile = self.start_coord_tl.as_tile(zoom_level)
        lasttile = self.start_coord_br.as_tile(zoom_level)

        # Get number of tiles
        columns = lasttile.column - firsttile.column + 1
        rows = lasttile.row - firsttile.row + 1
        no_tiles = columns * rows

        return firsttile, lasttile, rows, columns, no_tiles

    def build_image_async(self, zoom=None, testing=False):
        """
        Step 2.
        Gets map tiles and aggregates ASYNCRONOUSLY
        :param zoom: 0-22.  High zoom levels will increase the time and download cost of this operation.
        :return: None
        """

        if not isinstance(self.start_coord_tl, Coordinate) or not isinstance(self.start_coord_tl, Coordinate):
            msg = ('Bounds of the map must be defined before the map can'
                   ' be built. Use "define_bounds()" or provide a KML with at least 2 "limit" points')
            self.log.error(msg)
            raise TMapException(msg)

        # 1. Determine Zoom level
        if not zoom:
            self.log.info('No zoom level set. Calculating ... ')
            no_tiles = 0
            zoom = 2
            while no_tiles < 12:  # Good for A4
                zoom += 1
                firsttile, lasttile, rows, columns, no_tiles = self._count_tiles(zoom)
                self.log.debug('Zoomlevel {}, gets {} tiles.'.format(zoom, no_tiles))
        else:
            firsttile, lasttile, rows, columns, no_tiles = self._count_tiles(zoom)
            if no_tiles < 18:
                msg = (u"At this zoom level ({}) only {} tiles will be used. "
                       u"This may produce a poor quality image.".format(zoom, no_tiles))
                self.log.warning(msg)
            if no_tiles > 36:
                msg = ("At this zoom level ({}) {} tiles will be used!! "
                       "This will result in a large download and produce an image that must "
                       "be scaled down by >3 times to fit on an A4 page. We reccomend using"
                       "the automatically determined zoom level".format(zoom, no_tiles))
                self.log.warning(msg)

        self.log.info(u'Attempting to build map with zoom level: {:d}'.format(zoom))
        self.log.info(u'{} tiles will be downloaded and stiched ...'.format(no_tiles))

        # 2.  Generate a base image
        max_tiles = 130
        if no_tiles > max_tiles:
            msg = (u'Zoom level too large, lower it to reduce the total tiles to bellow {}. '
                   u'N.B A 24 tile image must be scaled to 50% to fit on an A4 page').format(max_tiles)
            self.log.error(msg)
            raise TMapException(msg)

        joined_image = Image.new("RGBA", (self.tile_width*columns, self.tile_height*rows))

        # 3.  Get map tiles

        # Get something like promises in node, lots of slow web requests here
        count = 0  # Tile counter
        rs = []
        urls = []
        pix_coords = []
        for c in range(columns):
            col = firsttile.column + c

            for r in range(rows):
                row = firsttile.row + r
                count += 1
                urls.append(self._get_tile_url(zoom, col, row))
                rs.append(grequests.get(urls[-1]))
                pix_coords.append((c*self.tile_width, r*self.tile_height))

        def request_error(*args, **kwargs):
            msg = "Tile request failted !!"
            self.log.error(msg)
            self.log.error(f"args: {args}")
            self.log.error(f"kwargs: {kwargs}")
            raise TMapException(msg)

        if not testing:
            rqs = grequests.map(rs, exception_handler=request_error, size=20, gtimeout=240)

            # Resolve all webrequests and stick the image together
            for count, (r, pix_coord, url) in enumerate(zip(rqs, pix_coords, urls)):
                # Stick them on the base image

                ## Method 1
                # f = tempfile.TemporaryFile()  # Create a tempfile to download the image to
                # dat = r.content
                # f.write(dat)  # write the image data into the file
                # img = Image.open(f)  # Create a Pillow image from the file

                ## Method 2
                try:
                    dat = r.content
                except AttributeError:
                    msg = "Timeout reached. Check network connection or reduce zoom level ..."
                    self.log.error(msg)
                    raise TMapException(msg)
                except OSError:
                    msg = "Web request failed!! If this is the first tile request your server" \
                        " address may be wrong. Otherwise there may be network or throttling" \
                        " issues"
                    self.log.error(msg)
                    raise TMapException(msg)

                # b'{"message":"Invalid authentication credentials"}\n'  # Using bad API keys
                try:
                    stream = io.BytesIO(dat)
                    img = Image.open(stream)
                    r.close()
                except OSError:
                    msg = f"Web request failed!! \ndat: {dat}\nurl: {url}"
                    self.log.error(msg)
                    raise TMapException(msg)


                # Stick new image onto the baseimage
                joined_image.paste(img, pix_coord)
                self.log.debug(u'Stiching image {:3d} '.format(count))
                self.log.debug(u'http request: {}'.format(url))
                self.log.debug(u"at pixel coordinate: c: {:5d}px, r: {:5d}px".format(pix_coord[0], pix_coord[1]))

        # 4.  Build the Coordinate-pixel reference table
        self._build_pixel2coord_ref_table(rows, columns, firsttile, lasttile)

        # 5.  Writing on small images looks shitty. Increase to minimum size
        mag = MIN_MAP_SIDE_PXS / min(joined_image.size)
        joined_image = joined_image.resize((int(mag * joined_image.width), int(mag * joined_image.height)))

        # 6.  Resize the Coordinate-pixel reference table to match image dimensions
        self._enlarge_px_ref_tables(mag, joined_image.size)

        # Save class variables
        self.raw_map_image = joined_image
        self.modified_map_image = joined_image.copy()
        self._zoom = zoom
        self._magnification_factor = mag

        self.log.info('Image combined!')
        return

    def _enlarge_px_ref_tables(self, magnification_factor, new_image_size):
        def resize_array(arr, factor):
            # res, space = np.linspace(arr[0], arr[-1], num=factor * arr.size - int(factor), retstep=True)
            # end_arr = [res[-1] + (i + 1) * space for i in range(int(factor))]
            # new_arr = np.append(res, end_arr)
            # return new_arr
            # Not sure what happened here ^ but started getting errors from wobby.kml with it
            res, space = np.linspace(arr[0], arr[-1], num=int(factor * arr.size), retstep=True)
            return res

        # Need to bulk out pixel ref tables - so that it is the correct length
        tmp1 = resize_array(self.longditude_at_pixel, magnification_factor)
        self.longditude_at_pixel = tmp1
        tmp2 = resize_array(self.latitude_at_pixel, magnification_factor)
        self.latitude_at_pixel = tmp2

        # Check for resize problem
        # import ipdb; ipdb.set_trace()
        if ((self.latitude_at_pixel.size != new_image_size[1]) or
            (self.longditude_at_pixel.size != new_image_size[0])):
            msg = ("Image resized failed. Resize factor: {}.  New image size is {}"
                   "yet latitude reference array has length {} and longditude "
                   "reference array has length {}".format(magnification_factor,
                    new_image_size, self.latitude_at_pixel.size, self.longditude_at_pixel.size))
            self.log.error(msg)
            raise TMapException(msg)

        return

    def _build_pixel2coord_ref_table(self, row_count, column_count, firsttile, lasttile):
        # 1. Convert Tile back to Coordinate again ... compare
        cnr_pointA = Coordinate.tile2deg(firsttile)
        cnr_pointB = Coordinate.tile2deg(lasttile)

        # 2. Get Lat-delta and Long-delta. (!! remember to use rows-1 and cols-1 because far point is inside)
        # set each value of the list to: start + delta*index
        try:
            lat_delta = (cnr_pointB.as_degree().northing['degrees'] - cnr_pointA.as_degree().northing['degrees']
                         ) / ((row_count - 1) * self.tile_height)  # per pixel
            long_delta = (cnr_pointB.as_degree().easting['degrees'] - cnr_pointA.as_degree().easting['degrees']
                          ) / ((column_count - 1) * self.tile_width)  # minus 1 because pointB is top left corner
        except ZeroDivisionError:
            msg = ("Zoom level is too low.  Only one tile is returned. Use automatic zoom"
                   " level or choose one that requests more than one tile")
            self.log.error(msg)
            raise TMapException(msg)

        # TODO: Vectorise this with numpy
        self.latitude_at_pixel = np.array([cnr_pointA.as_degree().northing['degrees'] + i * lat_delta for i in range(self.tile_height * row_count)])
        self.longditude_at_pixel = np.array([cnr_pointA.as_degree().easting['degrees'] + i * long_delta for i in range(self.tile_width * column_count)])

        # 4. Update top and bottom corner coordinates
        self.start_coord_tl = cnr_pointA
        # self.start_coord_br = cnr_pointB  # TODO: NOT VALID YET

        self.log.info("Successfully built Pixel-Coordinate reference table ..." )
        return

    def coord_to_pixel(self, coord):
        # TODO: optimise with binary search and posibly numba

        if not isinstance(coord, Coordinate):
            msg = "Input point must be of type 'Coordinate'"
            self.log.error(msg)
            raise TMapException(msg)

        lat = coord.as_degree().northing['degrees']
        long = coord.as_degree().easting['degrees']

        # If we have magnified or shrunk the image the pixel must be adjusted
        e_px = TMap._find_nearest_index(self.longditude_at_pixel, long)
        n_px = TMap._find_nearest_index(self.latitude_at_pixel, lat)

        if e_px < 0 or n_px < 0:
            return None

        offset = self.border_pxs + self.dms_bord_width
        return e_px + offset, n_px + offset

    def _check_pix_ref_created(self):
        if self.latitude_at_pixel.size == 0 or self.longditude_at_pixel.size == 0:
            msg = ("Image must be built with 'build_image' to make this function available ...")
            self.log.error(msg)
            raise TMapException(msg)
        return True

    @staticmethod
    def _find_nearest_index(series, value):
        def search(truth_function, series):
            for i, v in enumerate(series):
                if truth_function(v):
                    return i - 1  # for consistency but could just be i
            return -1

        if series[0] < value < series[-1]:
            return search(lambda v: value < v, series)
        elif series[-1] < value < series[0]:
            return search(lambda v: value > v, series)
        else:  # outside range
            return -1

    def save_map(self, filename, file_type='JPEG'):
        if not self.modified_map_image:
            msg = ("Map not created!")
            self.log.error(msg)
            raise TMapException(msg)
        else:
            if filename[3:] != file_type.lower():
                filename = filename + '.' + file_type.lower()

            if filename[0] == '~':
                fn = os.path.expanduser(filename)
            else:
                fn = os.path.abspath(filename)

            self.modified_map_image.save(fn, file_type)
            self.log.info(u'Saving combined map to: {}'.format(fn))
        return

    def show_map(self):
        self.modified_map_image.show()

    def _get_tile_url(self, zoom, column, row):
        """
        Requires zoom level to be set already
        :param column: int,
        :param row: int
        :return: PIL.Image object in memory
        """

        if (not isinstance(column, int)) or (not isinstance(row, int)):
            msg = ('incorrect useage! Supply integer columns and rows ...')
            self.log.error(msg)
            raise TMapException(msg)

        url = self.get_tile_request_path(zoom, column, row)

        return url

    def _calculate_degree_grid_spacing(self, min_grids=3):
        lat_diff = self.latitude_at_pixel[-1] - self.latitude_at_pixel[0]
        long_diff = self.longditude_at_pixel[-1] - self.longditude_at_pixel[0]

        if abs(lat_diff) < abs(long_diff):
            deg_range = self.latitude_at_pixel
            other_range = self.longditude_at_pixel
        else:
            deg_range = self.longditude_at_pixel
            other_range = self.latitude_at_pixel

        # Get the first whole degree values
        dp, grid_points, grid_space = TMap._get_whole_values_in_series(deg_range, min_grids)

        # Get the whole degree values on the other axis using the same spacing
        other_points = TMap._get_points_from_series(other_range, grid_space)

        # TODO: define these earlier and use the first if statement
        if deg_range is self.latitude_at_pixel:
            lat_grid_points = grid_points
            long_grid_points = other_points
        else:
            long_grid_points = grid_points
            lat_grid_points = other_points

        return lat_grid_points, long_grid_points, grid_space, dp

    @staticmethod
    def get_place_first_sig_fig(num):
        return int(np.floor(np.log10(abs(num))))

    @staticmethod
    def _get_whole_values_in_series(series, min_len):
        """
        :param series:
        :param min_len:
        :return: A series of whole values at least 'min_len' long
        """

        diff = series[-1] - series[0]

        # 2. get a reasonable starting point
        # Get the decimal place of the first significant figure
        # TODO: Remove and replace with class function
        get_place_first_sig_fig = lambda x: int(np.floor(np.log10(abs(x))))

        startin_sig_fig = get_place_first_sig_fig(diff)
        # Try reducing the grid space until we have just enough points
        for sf in count(start=startin_sig_fig, step=-1):
            found = False
            for j in [5, 2, 1]:
                # Try largest grid space
                grid_space = j * 10 ** sf
                grid_points = TMap._get_points_from_series(series, grid_space)
                if len(grid_points) >= min_len:
                    found = True
                    break
            if found:
                break

        # TODO: check if all three of these are required
        return sf, grid_points, grid_space

    @staticmethod
    def _get_points_from_series(series, grid_space):
        """
        :param grid_space:
        :param series:
        :return: Whole multiples of 'grid_space' within 'series
        """

        # TODO: if this is only used once, consider putting this inside the caller

        sign = +1 if series[-1] > series[0] else -1

        # builds up a big range and then filters out available ones
        get_valid = lambda arr: arr[(arr < np.max(series)) & (arr > np.min(series))]

        # int() is like floor() if it distregared sign
        start = int(series[0]/grid_space) * grid_space  # subtract to ensure we start before map starts
        points = get_valid(np.arange(start=start, stop=series[-1], step=sign*grid_space))
        return points

    def calc_degree_grids(self, min_grids=3):
        self._check_pix_ref_created()

        offset = self.border_pxs + self.dms_bord_width

        # 2. Determine a grid spacing
        lat_grids, long_grids, spacing, dp = self._calculate_degree_grid_spacing(min_grids)

        self.log.debug(u'Grid spacing determined to be: {} degrees'.format(spacing))

        # Convert to pixel coordinates, draw line and label it
        lon_start_px = []
        lon_end_px = []
        lon_lab = []
        for long in long_grids:
            long_px = TMap._find_nearest_index(self.longditude_at_pixel, long) + offset
            if long_px < offset:  # Skip if not on map
                continue

            lon_start_px.append((long_px, 0+offset))
            lon_end_px.append((long_px, len(self.latitude_at_pixel)+offset))

        lat_start_px = []
        lat_end_px = []
        lat_lab = []
        for lat in lat_grids:
            lat_px = TMap._find_nearest_index(self.latitude_at_pixel, lat) + offset
            if lat_px < offset:  # Skip if not on map
                continue

            lat_start_px.append((0+offset, lat_px))
            lat_end_px.append((len(self.longditude_at_pixel)+offset, lat_px))

        Lines = namedtuple('Lines', ['labels', 'start_px', 'end_px', 'lab_px'])

        self.log.info("Degree grids locations are defined ... ")

        self._deg_grids = namedtuple('GridResults', ['northing_vals', 'easting_vals'])(
            Lines(lat_grids, lat_start_px, lat_end_px, None),
            Lines(long_grids, lon_start_px, lon_end_px, None))
        return

    def add_grid(self, grid_type, color=(50, 50, 50, 150), label_position='inside'):
        if grid_type == 'utm':
            if not self._utm_grids:
                self.calc_utm_grids()
            grids = self._utm_grids
            nlabels = grids.northing_vals.labels
            elabels = grids.easting_vals.labels
        elif grid_type == 'degree':
            if not self._deg_grids:
                self.calc_degree_grids()
            grids = self._deg_grids

            diff = abs(grids.easting_vals.labels[1] - grids.easting_vals.labels[0])
            dp = TMap.get_place_first_sig_fig(diff)

            # Format labels for degrees
            nlabels = [u"{0:.{1}f}{2} N".format(num, max(0, -dp), DEGREE_SIGN) for num in grids.northing_vals.labels]
            elabels = [u"{0:.{1}f}{2} E".format(num, max(0, -dp), DEGREE_SIGN) for num in grids.easting_vals.labels]
        else:
            msg = u"Invalid 'degree_type' chosen: '{}' !!  Choose from: ['utm', 'degree']".format(grid_type)
            self.log.error(msg)
            raise TMapException(msg)

        lines = grids.northing_vals
        self._draw_and_label_lines(self.modified_map_image, 'vert', nlabels, lines.start_px, lines.end_px,
                                   lines.lab_px, lab_position=label_position, color=color)
        lines = grids.easting_vals
        self._draw_and_label_lines(self.modified_map_image, 'horiz', elabels, lines.start_px, lines.end_px,
                                   lines.lab_px, lab_position=label_position, color=color)

        return

    def calc_utm_grids(self, min_grids=3):
        self._check_pix_ref_created()

        width, height = len(self.longditude_at_pixel), len(self.latitude_at_pixel)
        offset = self.border_pxs + self.dms_bord_width

        # 1. Compare zones
        tl_extent = Coordinate(self.latitude_at_pixel[0], self.longditude_at_pixel[0])
        br_extent = Coordinate(self.latitude_at_pixel[-1], self.longditude_at_pixel[-1])

        # If spanning multiple zones, only mark the zone
        if (tl_extent.as_utm().zone_col != br_extent.as_utm().zone_col or
                    tl_extent.as_utm().zone_row != br_extent.as_utm().zone_row):

            self.log.debug("Map over multiple UTM zones.  Marking these only ...")

            # Define vectorized numpy functions to return zone information
            get_zone_col = np.vectorize(lambda lat, lon: Coordinate(lat, lon).as_utm().zone_col)
            get_zone_row = np.vectorize(lambda lat, lon: Coordinate(lat, lon).as_utm().zone_row)

            def get_label_location(pxs):
                # Put the label in the middle of the region
                if len(pxs) < 2:
                    lab_px = [int(np.max(pxs) / 2)]
                else:
                    offset = int((pxs[1] - pxs[0]) / 2)  # requires at least 2 values
                    lab_px = [px - offset if px < pxs[-1] else pxs[-2] + offset for px in pxs]
                return lab_px

            # Get horiz pixel of change of zone
            zone_cols = get_zone_col(self.latitude_at_pixel[0], self.longditude_at_pixel)
            lon_lab = np.unique(zone_cols)
            lon_px = [np.max(np.where(zone_cols == z))+offset for z in lon_lab]
            lon_lab_px = list(zip(get_label_location(lon_px), [0+offset]*len(lon_px)))
            lon_start_px = list(zip(lon_px, [0+offset]*len(lon_px)))
            lon_end_px = list(zip(lon_px, [height+offset]*len(lon_px)))

            zone_rows = get_zone_row(self.latitude_at_pixel, self.longditude_at_pixel[0])
            lat_lab = np.unique(zone_rows)[::-1]  # Reverse latitude array because values descend for increasing px
            lat_px = [np.max(np.where(zone_rows == z))+offset for z in lat_lab]
            lat_lab_px = list(zip([0+offset]*len(lat_px), get_label_location(lat_px)))
            lat_start_px = list(zip([0+offset]*len(lat_px), lat_px))
            lat_end_px = list(zip([width+offset]*len(lat_px), lat_px))

        else:  # spans single zone
            # TODO: Check with tile2deg that the pixel row is the same lat and that a pixel column in the same lon

            # Use existing methods to divide up the map on logical UTM boundries
            tl_extent = Coordinate(self.latitude_at_pixel[0], self.longditude_at_pixel[0])
            br_extent = Coordinate(self.latitude_at_pixel[-1], self.longditude_at_pixel[-1])

            # Get ranges - !! This will fail across a zone boundry !!
            northing_range = np.linspace(tl_extent.as_utm().northing, br_extent.as_utm().northing,
                                        num=height)
            easting_range = np.linspace(tl_extent.as_utm().easting, br_extent.as_utm().easting,
                                        num=width)

            # Get appropriate labels and spacing for UTM grids
            if height > width:
                _, lon_lab, spacing = TMap._get_whole_values_in_series(easting_range, min_len=min_grids)
                lat_lab = TMap._get_points_from_series(northing_range, spacing)
            else:
                _, lat_lab, spacing = TMap._get_whole_values_in_series(northing_range, min_len=min_grids)
                lon_lab = TMap._get_points_from_series(easting_range, spacing)

            lon_lab = list(lon_lab)
            lat_lab = list(lat_lab)

            # Get pixel for these values
            get_utm_ref = np.vectorize(lambda lat, lon: utm.from_latlon(lat, lon))

            ## Create pixel reference for UTMs
            utm_px_ref_top = get_utm_ref(self.latitude_at_pixel[0], self.longditude_at_pixel)[0]  # eastings
            utm_px_ref_bot = get_utm_ref(self.latitude_at_pixel[-1], self.longditude_at_pixel)[0]
            utm_px_ref_left = get_utm_ref(self.latitude_at_pixel, self.longditude_at_pixel[0])[1]  # northings
            utm_px_ref_right = get_utm_ref(self.latitude_at_pixel, self.longditude_at_pixel[-1])[1]

            ## VERTICAL GRID LINES
            lon_lab_px = None
            lon_start_px = []
            lon_end_px = []
            missing_labs = []
            for lab in lon_lab:
                # If either is not on the map, skip it
                s_px = TMap._find_nearest_index(utm_px_ref_top, lab)
                e_px = TMap._find_nearest_index(utm_px_ref_bot, lab)
                if min(s_px, e_px) < 0:
                    missing_labs.append(lab)
                    continue
                lon_start_px.append((s_px + offset, 0 + offset))
                lon_end_px.append((e_px + offset, height + offset))
            # Remove missing labels
            [lon_lab.remove(l) for l in missing_labs]

            ## HORIZONTAL GRID LINES
            lat_lab_px = None
            lat_start_px = []
            lat_end_px = []
            missing_labs = []
            for lab in lat_lab:
                s_px = TMap._find_nearest_index(utm_px_ref_left, lab)
                e_px = TMap._find_nearest_index(utm_px_ref_right, lab)
                if min(s_px, e_px) < 0:
                    missing_labs.append(lab)
                    continue
                lat_start_px.append((0 + offset, s_px + offset))
                lat_end_px.append((width + offset, e_px + offset))
            # Remove missing labels
            [lat_lab.remove(l) for l in missing_labs]

            # Clean off decimal digits !! Possibly a problem for extremely zoomed in images ... probly not
            lon_lab = list(map(int, lon_lab))
            lat_lab = list(map(int, lat_lab))

        self.log.info("UTM grids locations are defined ... ")

        DrawnLines = namedtuple('DrawnLines', ['labels', 'start_px', 'end_px', 'lab_px'])

        self._utm_grids = namedtuple('GridResults', ['northing_vals', 'easting_vals'])(
            DrawnLines(lat_lab, lat_start_px, lat_end_px, lat_lab_px),
            DrawnLines(lon_lab, lon_start_px, lon_end_px, lon_lab_px))
        return

    def _draw_and_label_lines(self, image, orientation, label, start_pixel, end_pixel, label_pixel=None,
                              color=(50, 50, 50, 150), label_height=None, lab_position='inside'):
        """
        Bulk drawer and labeler. All inputs except orientation must be lists.
        :param orientation: ['horiz', 'vert']
        :param label: List of line labels. [str]
        :param start_pixel: List of x,y coordinate tuples. [(x,y)]
        :param end_pixel: List of x,y coordinate tuples. [[x,y)]
        :param label_pixel: Optional.  Specific location of the label.  If not provided
        the start_pixel is used
        :param color: Optional. e.g 'grey'
        :return:
        """

        if orientation not in ['horiz', 'vert', '45cw']:
            msg = (u"this orientation is not supported: {}".format(orientation))
            self.log.error(msg)
            raise TMapException(msg)

        if lab_position == 'inside':
            label_positions = ['after', 'before']
        elif lab_position == 'outside':
            label_positions = ['after', 'before']
        else:
            msg = "Invalid lab_position '{}'! Chose from 'inside' or 'outside' ..."
            self.log.effor(msg)
            raise TMapException(msg)


        if orientation == 'vert':
            rotation = 90
        elif orientation == '45cw':
            rotation = -45
            orientation = None  # So that No centering happens later
        else:  # 'horiz'
            rotation = 0

        # TODO: add 'before'/'after' args for labels
        # This will allow printing on both ends and print outside the map itself
        if not label_pixel:
            label_pixel = [None]*len(start_pixel)

        draw = ImageDraw.Draw(image)
        # Label parameter
        if not label_height:
            label_height = int((max(image.size) / 100))
        label_width = int(round(label_height * 8))  # approximately the right length, expect ~6 chars
        label_size = (label_width, label_height)

        for label_text, s_px, e_px, lab_px in zip(label, start_pixel, end_pixel, label_pixel):
            draw.line([s_px, e_px], fill=color)
            if lab_px is None:
                self.add_label_to(image, unicode_str(label_text), s_px, label_size, rotation=rotation,
                                  center=orientation, position=label_positions[0])
                self.add_label_to(image, unicode_str(label_text), e_px, label_size, rotation=rotation,
                                  center=orientation, position=label_positions[1])
            else:
                self.add_label_to(image, str(label_text), lab_px, label_size, rotation=rotation,
                                  center=orientation, position=label_positions[0])

        return

    def add_label_to(self, image, text, location, size, rotation=0, color=(5, 33, 61),
                     center=None, font_size_points=None, position='after'):
        """
        :param image: Image object to add label to
        :param text: String.
        :param location: Tuple (x,y). Where to write label
        :param size: Tuple (w,h). Approximate size to give the label
        :param rotation: int/float. degree roation counter-clockwise
        :param color: Label text color.  RGBA or string
        :param center: [None, 'horiz', 'vert']. How to center the label about the location
        :param font_size_points: int. Will attempt to use this font size rather than determine
        an appropriate size.
        :param position: ['before', 'after']. Weather to write the label FROM location or up
        to location.
        :return:
        """

        if abs(rotation) > 90:
            msg = ("The maximum allowed rotation is 90 degrees in either"
                   "direction")
            self.log.error(msg)
            raise TMapException(msg)

        self.log.debug(u"Attempting to add label: {} at "
                       u"location: ({}, {})".format(text, location[0], location[1]))

        # 1. Create the label image to write onto
        label = Image.new('L', size)  # ATTN!! must be 'L'
        drawer = ImageDraw.Draw(label)  # Get drawer for this image

        fonts = ['Arial', 'Baskerville', 'Courier', 'Futura', 'Monaco']
        #fontspec = fonts[0]

        # load from package file
        fontspec = os.path.join(here, "resources", "arial.ttf")

        spacing = int(size[1]/15)

        if font_size_points:
            text_size_points = font_size_points
        else:
            # 2. Find a font size that will fit inside the label
            text_size_points = 0
            for i in count(1):  # counts infinitely till broken
                test_font = ImageFont.truetype(fontspec, size=i)
                text_size = drawer.multiline_textsize(text, font=test_font, spacing=spacing)
                # if label.size[1] < text_size[1]:  # Compare height
                if (label.size[0] < text_size[0]) or (label.size[1] < text_size[1]):  # Compare both
                    text_size_points = i - 2   # and reduce once we
                    break                         # have reached limit

        if text_size_points < 1:
            msg = "Size of label too small for text!! Provide larger size ..."
            self.log.error(msg)
            raise TMapException(msg)

        # Define font with decided size
        label_font = ImageFont.truetype(fontspec, size=text_size_points)

        # Get text width to enable centering if not filling the gap
        text_length, text_height = drawer.textsize(text, font=label_font)

        # Write text onto the label. ATTN: must be set to white for colorizer
        # drawer.text((0, 0), text, font=label_font, fill='white')
        drawer.multiline_text((0, 0), text, font=label_font, fill='white', spacing=spacing)

        # Rotate the label
        w = label.rotate(rotation, expand=1)

        # fix location for rotations. If the rotation is anti-clock
        # the text is in the lower left corner, whereas if the
        # rotation is clockwise, the text is in the upper left.
        if rotation > 0:
            location = (location[0], location[1] - w.height)

        if center == 'horiz':
            location = (int(location[0] - text_length/2), location[1])
        if center == 'vert':
            location = (location[0], int(location[1] + text_length/2))

        if position == 'before':
            if center == 'vert':  # move it left
                location = location[0] - int(round(text_height*1.30)), location[1]
            else:  # move it up
                location = location[0], location[1] - int(round(text_height*1.25))

        # Superimpose label onto the image using a mask to remove background
        background_color = (0, 0, 0)  # Not actually used since we mask it out
        image.paste(ImageOps.colorize(w, background_color, color), location, w)

        return text_length, text_size_points

    @staticmethod
    def draw_border(img, color='black'):
        top_left = (0, 0)
        bot_left = (0, img.height - 1)
        top_right = (img.width - 1, 0)
        bot_right = (img.width - 1, img.height - 1)

        draw = ImageDraw.Draw(img)
        draw.line([top_left, bot_left, bot_right, top_right, top_left], fill=color)

        return img

    def _create_legend(self):
        w = int(self.modified_map_image.width)
        h = int(w / 15)  # height of title bar

        # Set whitespace between entries
        h_buf = int(h/15)
        w_buf = int(w/60)

        # Get size and position of different areas
        # divide the title bar into 3 sections: 1 - title and stats, 2 - Scale, 3. declinations
        #w_bounds = [0, w/7, 2*w/7, 5*w/7, w]
        w_bounds = [0, w/6, 2*w/6, 2*w/3, w]
        region_px = [int(w) for w in w_bounds]

        # Create new image this size
        legend_img = Image.new("RGBA", (w, h), color='white')

        # 2. Write in file details
        end_px, font_size = self._ctb_add_details_block(legend_img, h, h_buf, w, w_buf, region_px)

        # 3. Get Grid spacing and Make Scale up
        self._ctb_add_scale_block(legend_img, h, h_buf, w, w_buf, region_px)

        # 1. Add compass point diagram and adjustments
        self._ctb_add_compass_adjustments(legend_img, h, h_buf, w, w_buf, region_px, font_size=font_size)

        return legend_img

    def declination(self):
        """
        :return: Degree to add to TN to get MN
        """
        # Caclulate declination
        return geomag.declination(dlon=self.longditude_at_pixel[len(self.longditude_at_pixel)//2],
                                  dlat=self.latitude_at_pixel[len(self.latitude_at_pixel)//2])

    def _ctb_add_compass_adjustments(self, legend_img, h, h_buf, w, w_buf, region_px, font_size=None):
        # Skip if a multizone map
        if tuple(self.start_coord_tl.as_utm())[-2:] != tuple(self.start_coord_br.as_utm())[-2:]:
            msg = "Not printing bearing adjustments for multi-zone map ..."
            self.log.info(msg)
            return 0, 0

        # 1. Place Compass points on Title bar and add declinations
        # compass_points = Image.open('/Users/minmac/Code/tmapper/tmapper/resources/CompassPoints.png')
        cwd = os.path.dirname(os.path.realpath(__file__))
        compass_points = Image.open(os.path.join(cwd, 'resources/CompassPoints-Simple.png'))
        cp_size = h  #- 2*h_buf
        sml_cpoints = compass_points.resize((cp_size, cp_size))
        legend_img.paste(sml_cpoints, (region_px[3] + w_buf, 0))

        # Add text fields
        loc = (region_px[3] + 2*w_buf + cp_size, int(h/4))  # start it a quater down because we set the font
        txt_size = (w - loc[0], h - 2*h_buf)
        dec = self.declination()
        gc = self.true2grid()
        ba = dec - gc
        txt = (u'TN - MN (Declination): {0:16.1f}{3}\n'
               u'GN - TN (Grid Convergence): {1:3.1f}{3}\n'
               u'GN - MN (Bearing Adjust):   {2:7.1f}{3}').format(dec, gc, ba, DEGREE_SIGN)

        text_length, font_size = self.add_label_to(legend_img, txt, loc, txt_size,
                                                   color='black', font_size_points=int(font_size*1.2))
        self.log.info("Added compass adjustments ...")
        return loc[0] + text_length, font_size

    def _ctb_add_details_block(self, legend_img, h, h_buf, w, w_buf, region_px):
        try:
            kml_fn = self.kml_file_name
        except AttributeError:
            kml_fn = ''

        self.log.debug("Adding first column of details ... ")
        detail_text1 = ("Name: {}\n"
                        "date: {}\n"
                        "kml: {}\n".format(self.map_name, datetime.datetime.now().date().isoformat(), kml_fn))
        loc = (region_px[0], h_buf)
        txt_size = (int(region_px[1] - region_px[0] - w_buf), h - 2*h_buf)  # illogical but fits better !!
        text_length, font_size = self.add_label_to(legend_img, detail_text1, loc, txt_size, color='black')

        def clean_coord(coord):
            uni_str = unicode_str(coord)
            return uni_str.replace(u'Coordinate(', u'').replace(u')', u'')

        self.log.debug("Adding second column of details ... ")
        # TODO: try an get degree sign in python2
        detail_text2 = (u"Top left:         {}\n"
                        u"Bottom right: {}\n"
                        u"Tile server: \n{}").format(clean_coord(self.start_coord_tl), clean_coord(self.start_coord_br),
                                                    next(self.server_url))

        # This next block is written in remaining space, NOT from its region_px, but immediately
        # after the first text and before the Scale starts
        loc = (text_length + w_buf, h_buf)
        txt_size = (int(region_px[2] - loc[0]), h - 2*h_buf)  # illogical but fits better !!
        text_length, font_size = self.add_label_to(legend_img, detail_text2, loc, txt_size, color='black')
        self.log.debug("Text at font size: {} spans from {} to {}".format(font_size, loc[0], loc[0] + text_length))
        return loc[0] + text_length, font_size

    def _ctb_add_scale_block(self, legend_img, h, h_buf, w, w_buf, region_px):
        if not self._utm_grids:
            self.calc_utm_grids()
        if not self._deg_grids:
            self.calc_degree_grids()

        ## 1. Draw a line for metric scale
        try:
            grid_lab_diff = abs(self._utm_grids.northing_vals.labels[1] - self._utm_grids.northing_vals.labels[0])
        except TypeError:
            self.log.info("Not printing UTM scale for Multi-Zone map ... ")
            return

        self.log.info("Adding metric scale ... ")

        grid_px_diff = self._utm_grids.northing_vals.start_px[1][1] - self._utm_grids.northing_vals.start_px[0][1]
        line_width = int(h/60)

        # Space the scale in the middle of the available region
        avail_width = region_px[3] - region_px[2]
        start_px = (avail_width - grid_px_diff) // 2

        loc = (region_px[2] + start_px, int(3*h/4))  # Top left where the rectangle starts
        br = (loc[0]+grid_px_diff, loc[1] + line_width)

        # Draw incrimental lines along the length
        drawer = ImageDraw.Draw(legend_img)
        drawer.rectangle([loc, br], fill='black')

        # Create label, start and end pixel arrays to pass to _draw_and_label_lines()
        labels = ['0']
        start_pxs = [loc]
        end_pxs = [(loc[0], loc[1] + 5*line_width)]
        for i in (10, 4, 2, 5/4, 1):
            labels.append((utils.format_number(str(grid_lab_diff/i))+'m').replace('000m','km'))
            start_pxs.append((loc[0] + int(round(grid_px_diff/i)), loc[1]))
            end_pxs.append((loc[0] + int(round(grid_px_diff/i)), loc[1] + 5*line_width))

        # self._draw_and_label_lines(legend_img, '45cw', labels, start_pxs, end_pxs, label_pixel=end_pxs,
        #                            label_height=int(h/10),  color='black')
        lab_height = h//10
        self._draw_and_label_lines(legend_img, 'horiz', labels, start_pxs, end_pxs, label_pixel=end_pxs,
                                   label_height=lab_height,  color='black')

        ## 2. Draw a line for DMS scale
        if self.dms_bord_width:
            loc = (loc[0], h//2)
            deg_diff = abs(self._deg_grids.easting_vals.labels[1] - self._deg_grids.easting_vals.labels[0])
            deg_width = self._deg_grids.easting_vals.start_px[1][0] - self._deg_grids.easting_vals.start_px[0][0]
            br = (loc[0] + deg_width, loc[1] + self.dms_bord_width)
            drawer.rectangle([loc, br], fill='black')

            # Add label
            deg_dist = utils.format_number("{:.6f}".format(deg_diff)) + DEGREE_SIGN  #+ '(' + Coordinate.stringify_deg(deg_diff) + ')'
            txt_size = (30*self.dms_bord_width, int(1.7*self.dms_bord_width))
            loc = (loc[0] + deg_width//2, int(loc[1] - int(2*self.dms_bord_width)))
            self.add_label_to(legend_img, deg_dist, loc, txt_size, color='black')

        ## 3. Print 'Scale' at top of this section
        loc = (int(3*w/7), h_buf)
        txt_size = (int(w/7), int(h/5))
        _ = self.add_label_to(legend_img, "Scale", loc, txt_size, color='black')

        return

    def true2grid(self):
        """
        UTM grids only align with True North (Line of longditude) at the central meridian. In
        the northern hemisphere a point to the west of the central meridian will have a grid
        that diverges west from True North, when moving north.  A point on the East of the central longditude
        will have a UTM grid that diverges East.

        The question is how you choose to describe the lines relative to each other.  If a line diverges
        west, then it also converges east.

        In the southern hemisphere, when moving north a point in the WEST has a grid that diverges EAST.

        The interpretation of the formula without the 'hemisphere factor' must require definition of
        what the sign means.  The Hemisphere factor returns an angle clockwise of True North that can
        be used to adjust a bearing.  If the returned number is -'ve, then the grid goes west of TN.
        :return: Degrees to add to TN to get GN
        """

        # 1. Get UTM Zones central meridian (lon)
        lon0 = self.start_coord_tl.central_meridian()

        # 2. Calc : γ = arctan [tan (λ - λ0) × sin φ]
        lon = self.start_coord_br.easting()
        lat = self.start_coord_br.northing()

        hemisphere_factor = 1 if lat < 0 else -1

        grid_convergence = np.arctan(np.tan(lon - lon0) * np.sin(lat)) * hemisphere_factor
        return grid_convergence

    def add_legend(self):
        if ((self.modified_map_image.height - self.raw_map_image.height)
                != (self.modified_map_image.width - self.raw_map_image.width)):
            self.log.warning("Looks like the legend has already been added! use 'erase_edits' to start again ...")
            return

        self.log.info("Adding legend and formatting for printing ...")
        self.modified_map_image = TMap.draw_border(self.modified_map_image)
        legend = self._create_legend()

        # Work out dimensions for new image that fits map, legend and a border
        w = self.modified_map_image.width
        h = self.modified_map_image.height + legend.height
        border_dist = int(h / 80)
        self.log.info("Adding a {}px border around Map ...".format(border_dist))
        w += 2*border_dist
        h += 2*border_dist

        # Create the base image
        img = Image.new("RGBA", (w, h), color='white')

        img.paste(self.modified_map_image, (border_dist, border_dist))
        img.paste(legend, (border_dist, border_dist+self.modified_map_image.height))
        self.log.info("Large base image created.  Map and Legend overlayed ...")

        # Set class variables accordingly
        self.modified_map_image = img
        self.border_pxs = border_dist

        return

    def add_dms_border(self):
        """Adds a small border to modified_map_image showing DMS.
        -1 from the end_pixel because it is going too far
        """

        if self.border_pxs != 0:
            msg = "!! Must add DMS border BEFORE adding legend !!"
            self.log.error(msg)
            TMapException(msg)

        bord_width = int(min(self.modified_map_image.size)/ 150)
        size = self.modified_map_image.width + 2*bord_width, self.modified_map_image.height + 2*bord_width

        new_img = Image.new('RGBA', size, color='white')
        drawer = ImageDraw.Draw(new_img)

        # Get pixel location of rectangles
        if not self._deg_grids:
            self.calc_degree_grids()

        ## Color the lines border in with rectangels

        # Northings
        labs = self._deg_grids.northing_vals.labels
        pts = len(labs)
        px = self._deg_grids.northing_vals.start_px
        if pts % 2 != 0:
            px.append((0, self.modified_map_image.height))
        # if odd number use end as final
        for i in range(0, pts, 2):
            px1 = px[i][1]
            px2 = px[i + 1][1]

            # left side
            s_px = 0, px1 + bord_width
            e_px = 0 + bord_width, px2 + bord_width - 1
            drawer.rectangle((s_px, e_px), fill='black')

            # right side
            s_px = new_img.width - bord_width, px1 + bord_width
            e_px = new_img.width, px2 + bord_width - 1
            drawer.rectangle((s_px, e_px), fill='black')

        # Eastings
        labs = self._deg_grids.easting_vals.labels
        pts = len(labs)
        px = self._deg_grids.easting_vals.start_px
        if pts % 2 != 0:
            px.append((self.modified_map_image.width, 0))
        # if odd number use end as final
        for i in range(0, pts, 2):
            px1 = px[i][0]
            px2 = px[i + 1][0]

            # top side
            s_px = px1 + bord_width, 0
            e_px = px2 + bord_width - 1, 0 + bord_width,
            drawer.rectangle((s_px, e_px), fill='black')

            # bottom side
            s_px = px1 + bord_width, new_img.height - bord_width,
            e_px = px2 + bord_width - 1, new_img.height
            drawer.rectangle((s_px, e_px), fill='black')

        # Save modified image
        new_img.paste(TMap.draw_border(self.modified_map_image), (bord_width, bord_width))
        self.modified_map_image = TMap.draw_border(new_img)
        self.dms_bord_width = bord_width  # Augment the width to be used later for counting

        return
