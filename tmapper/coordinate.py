
import math
import utm
from collections import namedtuple

import sys

# Only really need this for debugging in a terminal
if sys.version_info.major < 3:
    DEGREE_SIGN = u''
else:
    DEGREE_SIGN = u'\N{DEGREE SIGN}'

class CoordinateException(Exception):
    pass


class UTM(namedtuple('UTM', ['easting', 'northing', 'zone_col', 'zone_row'])):
    def __repr__(self):
        return (u"UTM(northing={0:.0f}m, easting={1:.0f}m, zone_col={2}, "
                u"zone_row='{3}')").format(self.northing, self.easting, self.zone_col, self.zone_row)

class Coordinate:

    Tile = namedtuple('Tile', 'zoomlvl, column, row')
    Coord = namedtuple('Coord', 'northing, easting')


    def __init__(self, northing, easting):
        """
        Currently only support initiation with Lat/Long in degrees
        :param northing (Latitude): degrees north eg. -28.18483
        :param easting (Longditude): degrees east. eg. 152.40706
        :return:
        """

        if not (isinstance(northing, (int, float)) and isinstance(easting, (int, float))
                and abs(northing) <= 90
                and abs(easting) <= 180):
            raise ValueError('Invalid values for Lat/Long ...')

        self._northing = northing
        self._easting = easting
        return

    def __repr__(self):
        return u"Coordinate({0:.4f}{2} N, {1:.4f}{2} E)".format(self._northing, self._easting, DEGREE_SIGN)

    def northing(self):
        return self.as_degree().northing['degrees']

    def easting(self):
        return self.as_degree().easting['degrees']

    def assertEqualTo(self, other_coord):

        lat_equal = self._northing == other_coord._northing
        long_equal = self._easting == other_coord._easting

        if not (lat_equal and long_equal):
            raise AssertionError("Coordinates NOT equal ...")

        return 0

    def as_tile(self, zoom):
        lat_rad = math.radians(self._northing)
        n = 2.0 ** zoom
        xtile = int((self._easting + 180.0) / 360.0 * n)
        ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
        return self.Tile(zoom, xtile, ytile)

    def as_degree(self):
        northing = {'degrees': self._northing}
        easting = {'degrees': self._easting}
        return self.Coord(northing, easting)

    def as_dms(self):
        northing = Coordinate.deg2dms(self._northing)
        easting = Coordinate.deg2dms(self._easting)
        return self.Coord(northing, easting)

    def as_degmin(self):
        northing = Coordinate.deg2degmin(self._northing)
        easting = Coordinate.deg2degmin(self._easting)
        return self.Coord(northing, easting)

    def as_utm(self):
        return UTM(*utm.from_latlon(self._northing, self._easting))

    def central_meridian(self):
        return (self.as_utm().zone_col - 1) * 6 - 180 + 3

    ################## CLASS METHODS ####################

    @staticmethod
    def stringify_deg(deg):
        return Coordinate.stringify_dms(Coordinate.deg2dms(deg))

    def stringify_dms(dms):
        sec = dms.get('seconds', None)
        min = dms.get('minutes', None)
        res = str(dms['degrees']) + DEGREE_SIGN
        if min: res + str(min) + "'"
        if sec: res + str(sec) + '"'
        return res

    @staticmethod
    def _usage_error():
        msg = ("To get a coordinates Northing (latitude) or easting (longditude) first"
               " choose an output form. E.g '.degrees()', '.dms()' etc ...")
        return msg

    @staticmethod
    def deg2dms(angle):
        deg, dmin = Coordinate._get_60ths(angle)
        minutes, dsec = Coordinate._get_60ths(dmin)
        return {'degrees': deg, 'minutes': minutes, 'seconds': dsec}

    @staticmethod
    def deg2degmin(angle):
        deg, dmin = Coordinate._get_60ths(angle)
        return {'degrees': deg, 'minutes': dmin}

    @staticmethod
    def _get_60ths(dec):
        whole = int(dec)
        rem = dec - whole
        return whole, rem * 60

    @classmethod
    def tile2deg(cls, tile):
        # Check input is a tile

        if not isinstance(tile, cls.Tile):
            raise TypeError("Must provide a tile object from 'create_tile' or 'as_tile'")

        n = 2.0 ** tile.zoomlvl
        lon_deg = tile.column / n * 360.0 - 180.0
        lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * tile.row / n)))
        lat_deg = math.degrees(lat_rad)
        return Coordinate(lat_deg, lon_deg)

    # @classmethod
    # def get_tile(cls, zoom, column, row):
    #     """
    #     Wraps input parameters as Tile object for consumption by other methods of Coordinate class
    #     and TMapper.  The system is explained: http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Python
    #     :param zoom: int 0-22
    #     :param column: int
    #     :param row: int
    #     :return: Tile object (namedtuple)
    #     """
    #
    #     if (zoom > 22 or zoom < 0) or not (isinstance(column, int) and isinstance(row, int)):
    #         raise ValueError('See docs for usage ...')
    #
    #     return cls.Tile(zoom, column, row)

